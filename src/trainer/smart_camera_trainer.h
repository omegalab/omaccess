#ifndef OM_SMARTCAMERATRAINER_H
#define OM_SMARTCAMERATRAINER_H

#include <filesystem>

#include <opencv2/opencv.hpp>

#include "dnn/face_aligner.h"
#include "dnn/face_detector.h"
#include "dnn/face_recognizer.h"
#include "dnn/face_svm.h"

#include "utils/helper.h"
#include "utils/options.h"

namespace fs = std::filesystem;

namespace omv {

class SmartCameraTrainer {
 public:
  SmartCameraTrainer(Options::Ptr opts);

  void Init();
  void Train();

 private:
  Options& opts_;
  iie::Core iie_core_;

  FaceDetector face_detector_;
  FaceAligner face_aligner_;
  FaceRecognizer face_recognizer_;
  FaceSVM face_svm_;

  std::pair<int, std::string> SplitPathName(const fs::path& path);

  void SortPaths(std::vector<fs::path>&);
  void ExtractFeatures(const fs::path& path, std::vector<float>&);
};

}  // namespace om

#endif  // OM_SMARTCAMERATRAINER_H
