#include <iostream>

#include "smart_camera_trainer.h"
#include "utils/logger.h"
#include "utils/options.h"

int main(int argc, char* argv[]) {
  int exit_code = EXIT_FAILURE;

  omv::Logger::Instance().init(10);
  auto options = omv::Options::Parse(argc, argv,         //
                                     omv::kCommonKeys,   //
                                     omv::kTrainerKeys,  //
                                     omv::kIeeKeys,      //
                                     omv::kSvmKeys       //
  );

  try {
    omv::Logger::Instance().SetSeverityLevel((*options)["verbose"]);
    omv::SmartCameraTrainer trainer(options);

    trainer.Init();
    trainer.Train();

    exit_code = EXIT_SUCCESS;
  } catch (std::exception& e) {
    LOG(omv::ERROR) << e.what();
  }

  return exit_code;
}
