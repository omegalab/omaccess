#include "smart_camera_trainer.h"

#include <filesystem>

#include "fmt/format.h"

#include "utils/logger.h"

namespace omv {

SmartCameraTrainer::SmartCameraTrainer(Options::Ptr opts)
    : opts_{*opts}, iie_core_{}, face_detector_{opts}, face_aligner_{opts}, face_recognizer_{opts}, face_svm_{opts} {
  BaseCNN::LoadIee(iie_core_);
}

void SmartCameraTrainer::Init() {
  face_detector_.Init(iie_core_);
  face_aligner_.Init(iie_core_);
  face_recognizer_.Init(iie_core_);
  face_svm_.Init();
}

void SmartCameraTrainer::Train() {
  namespace fs = std::filesystem;

  fs::path train_data_path;
  std::vector<fs::path> train_paths;

  if (opts_.Has("train-path"))
    train_data_path = fs::path(opts_.Get<std::string>("train-path"));
  else
    train_data_path = fs::current_path();

  for (const auto& entry : fs::directory_iterator(train_data_path))
    if (entry.is_directory())
      train_paths.push_back(entry.path());

  SortPaths(train_paths);

  std::vector<std::string> face_labels;
  FaceSVM::FeaturesMap face_features;

  std::vector<float> face_embeddings(256, 0);

  for (const auto& path : train_paths) {
    const auto& [id, label] = SplitPathName(path.filename());

    int extracted = 0;
    face_labels.push_back(label);

    for (const auto& image_p : fs::directory_iterator(path)) {
      ExtractFeatures(image_p, face_embeddings);

      if (!face_embeddings.empty()) {
        face_features.insert({id, face_embeddings});
        face_embeddings.clear();
        extracted++;
      }
    }

    LOG(INFO) << path.filename().string() << "; faces extracted: " << extracted;
  }

  CV_Assert(!face_features.empty());

  face_svm_.Train(face_labels, face_features);
  face_svm_.Save();
}

std::pair<int, std::string> SmartCameraTrainer::SplitPathName(const fs::path& path) {
  std::vector<std::string> tokens;

  try {
    std::istringstream tokens_stream(path);
    std::string token;

    while (std::getline(tokens_stream, token, '.'))
      tokens.push_back(token);

    if (tokens.size() != 2)
      throw(std::runtime_error(fmt::format("invalid path {}", path.string())));

  } catch (std::invalid_argument& e) {
    throw(std::runtime_error(fmt::format("{} invalid path {}", e.what(), path.string())));
  }

  return std::pair(std::stoi(tokens[0]), tokens[1]);
}

void SmartCameraTrainer::SortPaths(std::vector<fs::path>& paths) {
  std::sort(std::begin(paths), std::end(paths), [&](const fs::path& lhs, const fs::path& rhs) {
    const auto& [lhs_id, lhs_label] = SplitPathName(lhs.filename());
    const auto& [rhs_id, rhs_label] = SplitPathName(rhs.filename());

    return lhs_id < rhs_id;
  });
}

void SmartCameraTrainer::ExtractFeatures(const fs::path& image_p, std::vector<float>& face_embeddings) {
  std::vector<cv::Rect> face_boxes;

  cv::Mat face_landmarks = cv::Mat::zeros(5, 2, CV_32F);

  cv::Mat mat = cv::imread(image_p.string(), cv::IMREAD_COLOR);

  face_detector_.MakeAsyncRequest(mat);
  face_detector_.GetResults(face_boxes, mat.rows, mat.cols);

  if (face_boxes.size() != 1)
    return;

  //  cv::Mat face_mat = mat(face_boxes[0]);

  auto face = std::make_shared<Face>(mat, face_boxes[0]);

  face_aligner_.MakeAsyncRequest(face->m);
  face_aligner_.GetResults(face);
  face_aligner_.AlignFace(face);

  if (!face->m.empty()) {
    face_recognizer_.MakeAsyncRequest(face->m);
    face_recognizer_.GetResults(face_embeddings);
  } else
    LOG(DEBUG) << "failed aligning face: " << image_p.filename().string();
}

}  // namespace omv
