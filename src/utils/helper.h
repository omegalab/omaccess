#ifndef OM_HELPER_H
#define OM_HELPER_H

#include <sstream>
#include <string>
#include <vector>

namespace omv {

inline static const std::vector<std::string> kCommonKeys{
    "{help usage   ?   |                                   | print this message                       }",
    "{v verbose        | 3                                 | logging verbose level (0..3)             }"};

inline static const std::vector<std::string> kCameraKeys{
    "{i camera-device  | 0                                 | video camera device index                }",
    "{h capture-height | 480                               | camera capture height (multiple of 240)  }",
    "{w capture-width  | 640                               | camera captures width (multiple of 320)  }",
    "{i camera-device  | 0                                 | video camera device index                }",
    "{  analytics      | 1                                 | video camera device index                }",
    "{  rotate         | 0                                 | video camera device index                }",
    "{  reset-timer    | 60                                | video camera device index                }",
    "{o video-out      | 0                                 | video output (window-0/http-1/stdout-2)  }",
    "{ video-out-scheme| http                              | video output (window-0/http-1/stdout-2)  }",
    "{ video-out-host  | 127.0.0.1                         | video output (window-0/http-1/stdout-2)  }",
    "{ video-out-port  | 5050                              | video output (window-0/http-1/stdout-2)  }",
    "{ video-out-root  | send_frame                        | video output (window-0/http-1/stdout-2)  }"};

inline static const std::vector<std::string> kTrainerKeys{
    "{  train-path     |                                   | input directory path with train images   }"
    "{  test-data      | 0                                 | input directory path with train images   }"
    "{  test-ratio     | 0.90                              | input directory path with train images   }"
    "{o output-path    |                                   | output directory path to store svm-model }"};

inline static const std::vector<std::string> kIeeKeys{
    "{p models-path    | /usr/local/omaccess/models        | absolute path to models dir              }",
    "{d detection      | face-detection-adas-0001          | face detection model name                }",
    "{a alignment      | landmarks-regression-retail-0009  | face alignment model name                }",
    "{r recognition    | face-reidentification-retail-0095 | face recognition model name              }",
    "{m face-margin    | 1.1                               | margin to add to face rect               }",
    "{  align-margin   | 0                                 | use face margin when align               }",
    "{  age-gender     | age-gender-recognition-retail-0013| age and gender detector model            }",
    "{  emotions       | emotions-recognition-retail-0003  | emotions detector model                  }"};

inline static const std::vector<std::string> kSvmKeys{
    "{  svm-data       |                                   | SVM models path                          }"
    "{  svm-kernel     | rbf                               | SVM kernel type                          }",
    "{  svm-cost       | 100                               | SVM cost value                           }",
    "{  svm-gamma      | 0.001                             | SVM RBF kernel gamma                     }",
    "{  svm-degree     | 10                                | SVM POLY kernel degree                   }"};

inline static const std::vector<std::string> kApiKeys{
    "{  api-scheme     | http                              | API scheme (http/https)                  }",
    "{  api-host       | 127.0.0.1                         | API host                                 }",
    "{  api-port       | 8085                              | API port                                 }",
    "{  api-root       | api                               | API endpoint                             }"};

class Helper {
 public:
  Helper() = default;

  static float CalcDist(std::vector<float>& v1, std::vector<float>& v2);

  template <typename It>
  static std::string PrintRange(const It& begin, const It& end) {
    std::ostringstream os;
    for (auto it = begin; it != end; ++it) {
      os << *it;
    }
    return os.str();
  }
};

}  // namespace omv

#endif  // OM_HELPER_H
