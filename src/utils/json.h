#ifndef OM_JSON_H
#define OM_JSON_H

#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/rapidjson.h"
#include "rapidjson/writer.h"

namespace rj = rapidjson;

namespace omv {

class Json {
 public:
  Json();

  std::string Stringfy() const;
  void ParseFromFile(const std::string&);

  template <class Q>
  void Set(const char* key, Q value) {
    auto& allocator = doc_.GetAllocator();
    rj::Value json_key;
    rj::Value json_val;
    json_key.SetString(key, allocator);
    json_val = value;
    doc_.AddMember(json_key, value, allocator);
  }

  inline friend std::ostream& operator<<(std::ostream& os, const Json& J) {
    return os << J.Stringfy();
  }

  rj::Value& Get(const char* key) {
    return doc_[key];
  }

 private:
  rj::Document doc_;
};

}  // namespace omg

#endif  // OM_JSON_H
