#include "rest_client.h"

#include "fmt/format.h"
#include "utils/logger.h"

namespace omv {

RestClient::RestClient() {
  curl_ = curl_easy_init();
}

RestClient::~RestClient() {
  if (curl_ != nullptr)
    curl_easy_cleanup(curl_);
}

void RestClient::Init(Options& opts) {
  std::string scheme = opts["api-scheme"];
  std::string host = opts["api-host"];
  std::string port = opts["api-port"];
  std::string root = opts["api-root"];

  Init(fmt::format("{}://{}:{}/{}", scheme, host, port, root));
}

void RestClient::Init(const std::string& endpoint) {
  url_ = endpoint;

  LOG(INFO) << "using api url: " << url_;
}

void RestClient::HttpGet(const std::string& url, const RestClient::Handler& handler) {
  CURLcode res;

  std::string endpoint;

  if (url.empty())
    endpoint = url_;
  else
    endpoint = fmt::format("{}/{}", url_, url);

  if (curl_) {
    curl_easy_setopt(curl_, CURLOPT_URL, endpoint.c_str());
    curl_easy_setopt(curl_, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, &RestClient::curl_write);
    res = curl_easy_perform(curl_);

    if (res != CURLE_OK)
      LOG(ERROR) << "http get error: " << curl_easy_strerror(res) << " (" << endpoint << ')';

    else
      handler(http_response_);
  }
}

void RestClient::HttpPost(const std::string& url, Json&& json, const Handler& handler) {
  CURLcode res;
  CURL* curl = curl_easy_init();

  std::string endpoint;

  if (url.empty())
    endpoint = url_;
  else
    endpoint = fmt::format("{}/{}", url_, url);

  std::string data = json.Stringfy();

  struct curl_slist* headers = nullptr;
  headers = curl_slist_append(headers, "Content-Type: application/json");
  headers = curl_slist_append(headers, "charsets: utf-8");

  if (curl) {
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_easy_setopt(curl, CURLOPT_URL, endpoint.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, data.length());
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &RestClient::curl_write);
    res = curl_easy_perform(curl);

    if (res != CURLE_OK)
      LOG(ERROR) << "http post error: " << curl_easy_strerror(res) << " (" << endpoint << ')';

    else
      handler(http_response_);

    curl_easy_cleanup(curl);
  }
}

void RestClient::HttpPost(const std::string& url, const std::string& data, const RestClient::Handler& handler) {
  CURLcode res;
  CURL* curl = curl_easy_init();

  std::string endpoint;

  if (url.empty())
    endpoint = url_;
  else
    endpoint = fmt::format("{}/{}", url_, url);

  //  struct curl_slist* headers = nullptr;
  //  headers = curl_slist_append(headers, "Content-Type: application/json");
  //  headers = curl_slist_append(headers, "charsets: utf-8");

  if (curl) {
    http_response_.clear();
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_easy_setopt(curl, CURLOPT_URL, endpoint.c_str());
    //    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, data.length());
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &RestClient::curl_write);
    res = curl_easy_perform(curl);

    if (res != CURLE_OK)
      LOG(ERROR) << "http post error: " << curl_easy_strerror(res) << " (" << url.c_str() << ')';

    else
      handler(http_response_);

    curl_easy_cleanup(curl);
  }
}

size_t RestClient::curl_write(char* data, size_t size, size_t nmemb, void* stream) {
  auto rest_client = static_cast<RestClient*>(stream);
  return rest_client->curl_write_impl(data, size, nmemb);
}

size_t RestClient::curl_write_impl(char* data, size_t size, size_t nmemb) {
  http_response_.assign(data, size * nmemb);
  return size * nmemb;
}

}  // namespace omv
