#ifndef OM_RESTCLIENT_H
#define OM_RESTCLIENT_H

#include <curl/curl.h>

#include "json.h"
#include "options.h"

namespace omv {

class RestClient {
 public:
  using Handler = std::function<void(const std::string&)>;

  RestClient();
  ~RestClient();

  void Init(Options& opts);
  void Init(const std::string& endpoint);

  void HttpGet(const std::string& url, const Handler&);
  void HttpPost(const std::string& url, Json&& json, const Handler&);
  void HttpPost(const std::string& url, const std::string& data, const Handler&);

 private:
  CURL* curl_ = nullptr;

  std::string url_;
  std::string http_response_;

  static size_t curl_write(char* data, size_t size, size_t nmemb, void* stream);
  size_t curl_write_impl(char* data, size_t size, size_t nmemb);
};

}  // namespace omg

#endif  // OM_RESTCLIENT_H
