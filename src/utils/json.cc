#include "json.h"

#include <fstream>

#include "rapidjson/istreamwrapper.h"

#include "utils/logger.h"

namespace omv {

Json::Json() : doc_{} {
  doc_.SetObject();
}

std::string Json::Stringfy() const {
  rj::StringBuffer sb;
  rj::Writer<rj::StringBuffer> writer(sb);

  doc_.Accept(writer);

  return sb.GetString();
}

void Json::ParseFromFile(const std::string& file) {
  std::ifstream json_file(file);

  if (json_file.is_open()) {
    rj::IStreamWrapper isw(json_file);
    doc_.ParseStream(isw);
  }
}

}  // namespace omg
