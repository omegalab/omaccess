#ifndef LOGGER_H
#define LOGGER_H

#include <future>
#include <iomanip>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>
#include <vector>

#if __x86_64__
#define LOGGER_SETW 16
#else
#define LOGGER_SETW 8
#endif

namespace omv {

enum Severity { EGRX = -1, ERROR = 0, WARN, INFO, DEBUG };

class NullBuffer : public std::streambuf {
 public:
  int overflow(int c);
};

class NullStream : public std::ostream {
 public:
  NullStream();
  ~NullStream();

 private:
  NullBuffer m_sb;
};

class CoutStream : public std::ostream {
 public:
  CoutStream();
  ~CoutStream();
};

class Logger {
 public:
  Logger() {}

  static Logger& Instance() {
    static Logger logger;
    return logger;
  }

  Logger(Logger const&) = delete;
  Logger& operator=(Logger const&) = delete;

  void init(int severity) {
    if (severity < 4)
      severity_ = static_cast<Severity>(severity);
    else
      severity_ = Severity::DEBUG;
  }

  static std::string ts() {
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);

    char buffer[24];

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &tm);
    return std::string(buffer);
  }

  inline std::ostream& log(Severity severity) {
    auto t_id = std::this_thread::get_id();
    auto log_async = std::async(std::launch::async, [&, t_id]() -> std::ostream& {
      Lock();

      if (severity <= severity_)
        return cout_ << '[' << std::right << std::setfill('0') << std::setw(LOGGER_SETW)  //
                     << std::hex << t_id << ']'                                           //
                     << '[' << std::left << std::setfill(' ') << std::setw(5)             //
                     << std::dec << kLevels.at(severity) << ']';
      else
        return null_;
    });

    return log_async.get();
  }

  Severity GetSeverity() const {
    return severity_;
  }

  std::string GetLogLevel() const {
    return kLevels.at(severity_);
  }

  void SetSeverityLevel(Severity severity) {
    severity_ = severity;
  }

  void Lock() {
    mutex_.lock();
  }

  void UnLock() {
    mutex_.unlock();
  }

 private:
  Severity severity_;
  NullStream null_;
  CoutStream cout_;

  std::mutex mutex_;
  const std::map<Severity, std::string> kLevels{{Severity::DEBUG, "debug"},  //
                                                {Severity::INFO, "info"},    //
                                                {Severity::WARN, "warn"},    //
                                                {Severity::ERROR, "error"},
                                                {Severity::EGRX, "egrx"}};
};

class Endl {
 public:
  Endl() {}

  Endl(Severity message_severity) {
    if (message_severity > Logger::Instance().GetSeverity()) {
      enable_ = false;
    }
  }

  ~Endl() {
    if (enable_)
      std::cout << std::endl;

    Logger::Instance().UnLock();
  }

  friend std::ostream& operator<<(std::ostream& os, const omv::Endl&);

 private:
  bool enable_ = true;
};

std::ostream& operator<<(std::ostream& os, const omv::Endl&);
}  // namespace omv

inline std::string ComputeMethodName(const std::string& pretty_function) {
  std::string pretty = pretty_function;

  size_t om = pretty.find("om::");
  if (om != std::string::npos)
    pretty.erase(pretty.begin(), pretty.begin() + static_cast<int>(om + 4));

  size_t brackets = pretty.find_first_of('(');

  if (brackets != std::string::npos)
    pretty.replace(pretty.begin() + static_cast<int>(brackets + 1), pretty.end(), 1, ')');

  size_t space = pretty.find_last_of(' ');

  if (space != std::string::npos)
    pretty.erase(pretty.begin(), pretty.begin() + static_cast<int>(space + 1));

  return pretty;
}

#define VERY_PRETTY_NAME ComputeMethodName(__PRETTY_FUNCTION__)
#define LOG(x) omv::Logger::Instance().log(x) << omv::Endl(x) << '[' << VERY_PRETTY_NAME << ':' << __LINE__ << "] "
#define LOGX LOG(EGRX)

#endif  // LOGGER_H
