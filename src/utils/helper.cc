#include "helper.h"

#include <opencv2/opencv.hpp>

#include "utils/logger.h"

namespace omv {

float Helper::CalcDist(std::vector<float>& v1, std::vector<float>& v2) {
  cv::Mat m1 = cv::Mat(v1).reshape(256, 0);
  cv::Mat m2 = cv::Mat(v2).reshape(256, 0);

  double xy = m1.dot(m2);
  double xx = m1.dot(m2);
  double yy = m2.dot(m2);
  double norm = sqrt(xx * yy) + 1e-6;

  return static_cast<float>(1.0 - xy / norm);
}

}  // namespace omv
