#ifndef OM_OPTIONS_H
#define OM_OPTIONS_H

#include <algorithm>
#include <iostream>
#include <iterator>

#include <opencv2/core/utility.hpp>

namespace omv {

class Options {
 public:
  using Ptr = std::shared_ptr<Options>;

  explicit Options(int, char* [], const std::string&);
  //  static Ptr Parse(int, char* [], const std::string&);

  template <typename... Args>
  static Ptr Parse(int argc, char* argv[], Args... args) {
    std::ostringstream keys_stream;
    std::ostream_iterator<std::string> keys_stream_it(keys_stream);

    for (const auto& p : {args...})
      std::copy(std::begin(p), std::end(p), keys_stream_it);

    return std::make_shared<Options>(argc, argv, keys_stream.str());
  }

  class ProxyValue {
   public:
    ProxyValue(Options* opts, const std::string& k) : opts_{opts}, k_{k} {}

    template <typename T>
    operator T() {
      return opts_->Get<T>(k_);
    }

   private:
    Options* opts_;
    std::string k_;
  };

  ProxyValue operator[](const std::string& key) {
    return ProxyValue(this, key);
  }

  template <typename T>
  inline T Get(const std::string& key, bool space_delete = true) const {
    return data_.get<T>(key, space_delete);
  }

  inline bool Has(const std::string& key) const {
    return data_.has(key);
  }

  inline void PrintErrors() const;
  inline void PrintMessage() const;

 private:
  cv::CommandLineParser data_;
};

}  // namespace omg

#endif  // OM_OPTIONS_H
