#include "utils/options.h"

#include "utils/logger.h"

namespace omv {

Options::Options(int argc, char* argv[], const std::string& opt_keys) : data_(argc, argv, opt_keys) {
  if (!data_.check()) {
    PrintErrors();
    PrintMessage();
    throw std::runtime_error("bad command line arguments");
  }

  if (data_.has("help")) {
    PrintMessage();
    std::exit(0);
  }
}

void Options::PrintErrors() const {
  data_.printErrors();
}

void Options::PrintMessage() const {
  data_.printMessage();
}

}  // namespace om
