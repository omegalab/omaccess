#include "utils/logger.h"

namespace omv {

int NullBuffer::overflow(int c) {
  return c;
}

NullStream::NullStream() : std::ostream(&m_sb) {}

NullStream::~NullStream() {}

CoutStream::CoutStream() : std::ostream(std::cout.rdbuf()) {}

CoutStream::~CoutStream() {}

std::ostream& operator<<(std::ostream& os, const Endl&) {
  return os;
}

}  // namespace omg
