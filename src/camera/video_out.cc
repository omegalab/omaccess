#include "video_out.h"

#include "utils/base64.h"
#include "utils/logger.h"

namespace omv {

GuiVideoOut::GuiVideoOut() { cv::namedWindow("smart_camera", cv::WINDOW_AUTOSIZE); }

GuiVideoOut::~GuiVideoOut() { cv::destroyAllWindows(); }

void GuiVideoOut::Write(Frame::Ptr frame) {
  cv::imshow("smart_camera", frame->m);

  if (int k = cv::waitKey(5); k == 27)
    stopped_ = true;
}

bool GuiVideoOut::IsStopped() const { return stopped_; }

VideoOut::Ptr VideoOutFactory::Create(VideoOut::Type type, Options& opts, Io& io) { return (kRegistry.count(type)) ? kRegistry.at(type)(opts, io) : nullptr; }

HttpVideoOut::HttpVideoOut() : streamer_{7878} {
  cv::Mat m = cv::Mat();
  streamer_.write(m);
  m.release();

  streamer_thread_ = std::make_unique<std::thread>([&]() { streamer_.start(); });
}

HttpVideoOut::~HttpVideoOut() {
  if (streamer_thread_->joinable())
    streamer_thread_->join();
}

void HttpVideoOut::Write(Frame::Ptr frame) { streamer_.write(frame->m); }

bool HttpVideoOut::IsStopped() const { return stopped_; }

RestVideoOut::RestVideoOut(Options& opts_, Io& io) : io_{io} {
  std::string scheme = opts_["video-out-scheme"];
  std::string host = opts_["video-out-host"];
  std::string port = opts_["video-out-port"];
  std::string root = opts_["video-out-root"];

  rest_client_.Init(fmt::format("{}://{}:{}/{}", scheme, host, port, root));
}

void RestVideoOut::Write(Frame::Ptr frame) {
  std::vector<uchar> outbuf;
  std::vector<int> params;
  params.push_back(cv::IMWRITE_JPEG_QUALITY);
  params.push_back(20);

  cv::imencode(".jpg", frame->m, outbuf, params);

  std::string base64_encoded = fmt::format("data={}", base64_encode(&outbuf[0], static_cast<unsigned int>(outbuf.size())));

  rest_client_.HttpPost({}, base64_encoded, [](const std::string& /*res*/) { /*LOG(INFO) << res;*/ });
}

}  // namespace omg
