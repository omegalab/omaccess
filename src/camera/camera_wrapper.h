#ifndef OM_CAMERA_H
#define OM_CAMERA_H

#include <opencv2/opencv.hpp>

#ifdef SMART_CAMERA_USE_RASPICAM
#include <raspicam/raspicam.h>
#endif

#include "frame.h"
#include "utils/options.h"

namespace omv {

class Camera {
 public:
  Camera(Options::Ptr opts);

  void Init(int camera_idx);
  void Retrieve(Frame::Ptr frame);

  void Release();

  std::pair<int, int> GetResolution() const;

 private:
  Options& opts_;

#ifdef SMART_CAMERA_USE_RASPICAM
  raspicam::RaspiCam camera_;
#else
  cv::VideoCapture camera_;
#endif

  int width_;
  int height_;
  int fps_{0};
};

void operator>>(Camera& camera, Frame::Ptr frame);

}  // namespace omv

#endif  // OM_CAMERA_H
