#ifndef OM_SMARTCAMERA_H
#define OM_SMARTCAMERA_H

#include <inference_engine.hpp>
#include <opencv2/opencv.hpp>
#include <thread>

#include "aio/io.h"
#include "aio/signal_set.h"
#include "camera_wrapper.h"
#include "dnn/age_gender_detector.h"
#include "dnn/emotions_detector.h"
#include "dnn/face_aligner.h"
#include "dnn/face_detector.h"
#include "dnn/face_recognizer.h"
#include "dnn/face_registry.h"
#include "dnn/face_svm.h"
#include "utils/helper.h"
#include "utils/options.h"
#include "utils/rest_client.h"
#include "video_out.h"

namespace omv {

class SmartCamera {
 public:
  SmartCamera(Options::Ptr opts);
  ~SmartCamera();

  void Init();
  void Start();

 private:
  Options& opts_;
  iie::Core iie_core_;

  Io io_;
  SignalSet signals_;

  Camera camera_;

  FaceSVM face_svm_;
  FaceRegistry face_registry_;

  FaceDetector::Ptr face_detector_;
  FaceAligner::Ptr face_aligner_;
  FaceRecognizer::Ptr face_recognizer_;
  AgeGenderDetector::Ptr age_gender_detector_;
  EmotionsDetector::Ptr emotions_detector_;

  VideoOut::Ptr vout_;

  std::vector<std::string> labels_;
  std::unique_ptr<std::thread> camera_thread_;
  std::atomic_bool stopped_{false};
  uint16_t rotate_{0};
  bool analytics_{true};
};

}  // namespace omg

#endif  // OM_SMARTCAMERA_H
