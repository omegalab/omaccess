#include <ctime>
#include <iostream>

#include <algorithm>
#include <iterator>
#include <vector>

#include "smart_camera.h"
#include "utils/logger.h"
#include "utils/options.h"

int main(int argc, char* argv[]) {
  int exit_code = EXIT_FAILURE;

  omv::Logger::Instance().init(10);
  auto options = omv::Options::Parse(argc, argv,        //
                                     omv::kCommonKeys,  //
                                     omv::kCameraKeys,  //
                                     omv::kIeeKeys,     //
                                     omv::kSvmKeys,     //
                                     omv::kApiKeys      //
  );

  try {
    omv::Logger::Instance().SetSeverityLevel((*options)["verbose"]);
    omv::SmartCamera smart_camera_(options);

    smart_camera_.Init();
    smart_camera_.Start();

    exit_code = EXIT_SUCCESS;

  } catch (std::runtime_error& e) {
    LOG(omv::ERROR) << e.what();
  }

  return exit_code;
}
