#include "frame.h"

#include "fmt/format.h"

namespace omv {

Frame::Frame(int h, int w) { m = cv::Mat::zeros(h, w, CV_8UC3); }

void Frame::DisplayFaceBoxes() {
  if (faces.empty())
    return;

  for (auto& face : faces) {
    cv::Scalar color;
    (face->id == 0) ? color = cv::Scalar(0, 0, 255) : color = cv::Scalar(0, 255, 0);

    auto text_size = cv::getTextSize(face->name, cv::FONT_HERSHEY_DUPLEX, 0.7, 1, nullptr);

    cv::rectangle(m, face->r, color, 2, 4);
    cv::rectangle(m, cv::Point2f(face->r.x, face->r.br().y - text_size.height - 5),
                  cv::Point2f(face->r.x + text_size.width, face->r.br().y), color, cv::FILLED);
    cv::putText(m, face->name,                             //
                cv::Point(face->r.x, face->r.br().y - 3),  //
                cv::FONT_HERSHEY_DUPLEX, 0.7,              //
                cv::Scalar(255, 255, 255), 1);
  }
}

void Frame::DisplayFaceStats() {
  int text_y = 30;

  auto put_label = [&](const std::string& label, const cv::Scalar& color) {
    auto label_size = cv::getTextSize(label, cv::FONT_HERSHEY_DUPLEX, 0.6, 1, nullptr);
    cv::putText(m, label, cv::Point(10, text_y), cv::FONT_HERSHEY_DUPLEX, 0.6, color, 1);

    text_y += label_size.height + label_size.height / 2;
  };

  for (auto& face : faces) {
    cv::Scalar color;
    (face->id == 0) ? color = cv::Scalar(0, 0, 255) : color = cv::Scalar(0, 255, 0);

    std::string label = fmt::format("id: {:#02} name: {}; a: {}; g: {}; e: {}",  //
                                    face->id, face->name, face->age, face->gender, face->emotion);
    put_label(label, color);
  }
}

void Frame::Rotate(uint16_t angle) {
  cv::Point2f center(m.cols / 2.f, m.rows / 2.f);
  cv::Mat rotation_matrix = cv::getRotationMatrix2D(center, angle, 1.0);
  cv::warpAffine(m, m, rotation_matrix, cv::Size(m.cols, m.rows));
}

}  // namespace omv
