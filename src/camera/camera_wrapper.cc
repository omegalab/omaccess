#include "camera_wrapper.h"

#include "fmt/format.h"

#include "utils/logger.h"

namespace omv {

Camera::Camera(Options::Ptr opts) : opts_{*opts} {}

void Camera::Init(int camera_idx) {
#ifdef SMART_CAMERA_USE_RASPICAM
  camera_.setVideoStabilization(true);
  camera_.setExposure(raspicam::RASPICAM_EXPOSURE_ANTISHAKE);
  camera_.setAWB(raspicam::RASPICAM_AWB_AUTO);
  camera_.setCaptureSize(opts_["capture-width"], opts_["capture-height"]);
  if (!camera_.open()) {
#else
  if (!camera_.open(camera_idx)) {
#endif
    throw std::runtime_error(fmt::format("error opening the camera index {}", camera_idx));
  }

#ifdef SMART_CAMERA_USE_RASPICAM
  height_ = static_cast<int>(camera_.getHeight());
  width_ = static_cast<int>(camera_.getWidth());
#else
  camera_.set(cv::CAP_PROP_FRAME_HEIGHT, opts_["capture-height"]);
  camera_.set(cv::CAP_PROP_FRAME_WIDTH, opts_["capture-width"]);
  camera_.set(cv::CAP_PROP_FPS, opts_["capture-fps"]);
  height_ = static_cast<int>(camera_.get(cv::CAP_PROP_FRAME_HEIGHT));
  width_ = static_cast<int>(camera_.get(cv::CAP_PROP_FRAME_WIDTH));
  fps_ = static_cast<int>(camera_.get(cv::CAP_PROP_FPS));
#endif

  LOG(INFO) << "opened camera (WxH@fps): " << width_ << 'x' << height_ << '@' << fps_;
}

void Camera::Retrieve(Frame::Ptr frame) {
  camera_.grab();
#ifdef SMART_CAMERA_USE_RASPICAM
  unsigned char* frame_data = camera_.getImageBufferData();
  frame->m = cv::Mat(height_, width_, CV_8UC3, frame_data);
#else
  camera_.retrieve(frame->m);
#endif
  cv::flip(frame->m, frame->m, 1);
}

std::pair<int, int> Camera::GetResolution() const {
  return std::pair(height_, width_);
}

void Camera::Release() {
  camera_.release();
}

void operator>>(Camera& camera, Frame::Ptr frame) {
  frame->faces.clear();
  frame->ts = cv::getTickCount();
  camera.Retrieve(frame);
}

}  // namespace omv
