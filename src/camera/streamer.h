#pragma once

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#define PORT unsigned short
#define SOCKET int
#define HOSTENT struct hostent
#define SOCKADDR struct sockaddr
#define SOCKADDR_IN struct sockaddr_in
#define ADDRPOINTER unsigned int*
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define TIMEOUT_M 200000
#define NUM_CONNECTIONS 10

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include "opencv2/opencv.hpp"

#include "utils/logger.h"


namespace omv {

struct clientFrame {
  uchar* outbuf;
  ssize_t outlen;
  int client;
};

struct clientPayload {
  void* context;
  clientFrame cf;
};

class Streamer {
  SOCKET sock;
  fd_set master;
  int timeout;
  int quality;  // jpeg compression [1..100]
  std::vector<int> clients;
  pthread_t thread_listen, thread_write;
  pthread_mutex_t mutex_client = PTHREAD_MUTEX_INITIALIZER;
  //  pthread_mutex_t mutex_cout = PTHREAD_MUTEX_INITIALIZER;
  pthread_mutex_t mutex_writer = PTHREAD_MUTEX_INITIALIZER;
  cv::Mat last_frame_;
  uint16_t port_;
  bool stopped_ = false;

  ssize_t _write(int sock, char* s, size_t len) {
    if (len < 1) {
      len = strlen(s);
    }
    {
      try {
        ssize_t retval = ::send(sock, s, len, 0x4000);
        return retval;
      } catch (int e) {
        LOG(ERROR) << "An exception occurred. Exception Nr. " << e;
      }
    }
    return -1;
  }

  ssize_t _read(int socket, char* buffer) {
    ssize_t result = recv(socket, buffer, 4096, MSG_PEEK);
    if (result < 0) {
      LOG(ERROR)<< "An exception occurred. Exception Nr. " << result << '\n';
      return result;
    }
    //    string s = buffer;
    //    buffer = (char*)s.substr(0, static_cast<size_t>(result)).c_str();
    std::string s = std::string(buffer, static_cast<size_t>(result));
    buffer = static_cast<char*>(&s[0]);
    return result;
  }

  static void* listen_Helper(void* context) {
    (static_cast<Streamer*>(context))->Listener();
    return nullptr;
  }

  static void* writer_Helper(void* context) {
    (static_cast<Streamer*>(context))->Writer();
    return nullptr;
  }

  static void* clientWrite_Helper(void* payload) {
    void* ctx = (static_cast<clientPayload*>(payload))->context;
    struct clientFrame cf = (static_cast<clientPayload*>(payload))->cf;
    (static_cast<Streamer*>(ctx))->ClientWrite(cf);
//    return nullptr;
  }

 public:
  Streamer(uint16_t port = 0) : sock(INVALID_SOCKET), timeout(TIMEOUT_M), quality(90), port_(port) {
    signal(SIGPIPE, SIG_IGN);
    FD_ZERO(&master);
    // if (port)
    //     open(port);
  }

  ~Streamer() {
    release();
  }

  bool release() {
    if (sock != INVALID_SOCKET)
      shutdown(sock, 2);
    sock = (INVALID_SOCKET);
    return false;
  }

  bool open() {
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    SOCKADDR_IN address;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_family = AF_INET;
    address.sin_port = htons(port_);
    // TODO: fix casting
    if (::bind(sock, reinterpret_cast<SOCKADDR*>(&address), sizeof(SOCKADDR_IN)) == SOCKET_ERROR) {
      LOG(ERROR) << "error : couldn't bind sock " << sock << " to port " << port_ << "!";
      return release();
    }
    if (listen(sock, NUM_CONNECTIONS) == SOCKET_ERROR) {
      LOG(ERROR)<< "error : couldn't listen on sock " << sock << " on port " << port_ << " !";
      return release();
    }
    FD_SET(sock, &master);
    return true;
  }

  bool isOpened() {
    return sock != INVALID_SOCKET;
  }

  void start() {
    pthread_mutex_lock(&mutex_writer);
    pthread_create(&thread_listen, nullptr, this->listen_Helper, this);
    pthread_create(&thread_write, nullptr, this->writer_Helper, this);
  }

  void stop() {
    stopped_ = true;
    this->release();
    pthread_join(thread_listen, nullptr);
    pthread_join(thread_write, nullptr);
  }

  void write(cv::Mat& frame) {
    pthread_mutex_lock(&mutex_writer);
    if (!frame.empty()) {
      last_frame_.release();
      last_frame_ = frame.clone();
    }
    pthread_mutex_unlock(&mutex_writer);
  }

 private:
  void Listener();
  void Writer();
  [[noreturn ]] void ClientWrite(clientFrame& cf);
};

}
