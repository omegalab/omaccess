#include "smart_camera.h"

#include <filesystem>
#include <numeric>

#include <opencv2/core.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "fmt/format.h"
#include "utils/logger.h"

namespace omv {

SmartCamera::SmartCamera(Options::Ptr opts)
    : opts_{*opts}
    , signals_{io_, SIGINT, SIGTERM, SIGHUP}
    , camera_{opts}
    , face_svm_{opts}
    , face_registry_{opts, io_}
    , camera_thread_{nullptr} {
  analytics_ = static_cast<bool>(opts_.Get<int>("analytics"));

  if (analytics_) {
    BaseCNN::LoadIee(iie_core_);
    face_detector_ = std::make_unique<FaceDetector>(opts);
    face_aligner_ = std::make_unique<FaceAligner>(opts);
    face_recognizer_ = std::make_unique<FaceRecognizer>(opts);
    age_gender_detector_ = std::make_unique<AgeGenderDetector>(opts);
    emotions_detector_ = std::make_unique<EmotionsDetector>(opts);
  }
}

SmartCamera::~SmartCamera() {
  if (camera_thread_ && camera_thread_->joinable())
    camera_thread_->join();
}

void SmartCamera::Init() {
  io_.Init();
  camera_.Init(opts_["camera-device"]);

  if (analytics_) {
    face_detector_->Init(iie_core_);
    face_aligner_->Init(iie_core_);
    face_recognizer_->Init(iie_core_);
    age_gender_detector_->Init(iie_core_);
    emotions_detector_->Init(iie_core_);

    face_svm_.Load();
    face_registry_.Init();
  }

  signals_.AsyncWait([&](int sig) {
    LOG(INFO) << "received signal:" << sig;
    stopped_ = true;
    io_.Stop();
  });

  rotate_ = static_cast<uint16_t>(opts_.Get<int>("rotate"));

  vout_ = VideoOutFactory::Create(opts_.Get<VideoOut::Type>("video-out"), opts_, io_);
}

void SmartCamera::Start() {
  camera_thread_ = std::make_unique<std::thread>([&]() {
    auto [height, width] = camera_.GetResolution();
    auto frame = std::make_shared<Frame>(height, width);

    std::vector<cv::Rect> face_boxes;

    while (!stopped_) {
      if (analytics_) {
        face_detector_->MakeAsyncRequest(frame->m);

        for (auto& face_box : face_boxes) {
          auto face = std::make_shared<Face>(frame->m, face_box);

          face_aligner_->MakeAsyncRequest(face->m);
          face_aligner_->GetResults(face);
          face_aligner_->AlignFace(face);

          face_recognizer_->MakeAsyncRequest(face->m);
          age_gender_detector_->MakeAsyncRequest(face->m);
          emotions_detector_->MakeAsyncRequest(face->m);

          age_gender_detector_->GetResults(face);
          emotions_detector_->GetResults(face);
          face_recognizer_->GetResults(face);

          face_svm_.Predict(face);

          if (face->id > 0)
            face_registry_.RegisterFace(face);

          frame->faces.push_back(face);
        }

        //      face_registry_.Display(frame);
        vout_->AddFrameStats(frame);
      }

      if (vout_)
        vout_ << frame;

      camera_ >> frame;

      if (rotate_)
        frame->Rotate(rotate_);

      if (analytics_) {
        face_boxes.clear();
        face_detector_->GetResults(face_boxes, height, width);
      }

      if (vout_ && vout_->IsStopped()) {
        io_.Stop();
        break;
      }
    }

    camera_.Release();
  });

  io_.Run();
}

}  // namespace omv
