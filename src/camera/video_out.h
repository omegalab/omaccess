#ifndef OM_VIDEOOUTPUT_H
#define OM_VIDEOOUTPUT_H

#include <opencv2/opencv.hpp>
#include <thread>

#include "aio/io.h"
#include "fmt/format.h"
#include "frame.h"
#include "streamer.h"
#include "utils/rest_client.h"

namespace omv {

class VideoOut {
 public:
  using Ptr = std::shared_ptr<VideoOut>;

  enum class Type { NONE = -1, WINDOW = 0, HTTP, STDOUT, REST };

  VideoOut() = default;
  virtual ~VideoOut() = default;

  virtual bool IsStopped() const = 0;
  virtual void Write(Frame::Ptr) = 0;

  void AddFrameStats(Frame::Ptr frame) {
    frame->DisplayFaceBoxes();
    frame->DisplayFaceStats();

    frame->te = (static_cast<double>(cv::getTickCount()) - frame->ts) / cv::getTickFrequency();
    frame->fps = 1 / frame->te;

    auto label = fmt::format("fps: {:.{}f}", frame->fps, 2);
    auto text_size = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.7, 2, nullptr);

    cv::putText(frame->m, label,                                      //
                cv::Point(frame->m.cols - text_size.width - 10, 30),  //
                cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0, 255, 0), 2);
  }

  friend inline void operator<<(VideoOut::Ptr vout, Frame::Ptr frame) { vout->Write(frame); }
};

class GuiVideoOut : public VideoOut {
 public:
  using Ptr = std::shared_ptr<GuiVideoOut>;

  GuiVideoOut();
  virtual ~GuiVideoOut() override;

  virtual void Write(Frame::Ptr) override;
  virtual bool IsStopped() const override;

  static Ptr Create(Options& /*opts*/, Io& /*io*/) { return std::make_shared<GuiVideoOut>(); }

 private:
  bool stopped_ = false;
};

class HttpVideoOut : public VideoOut {
 public:
  using Ptr = std::shared_ptr<HttpVideoOut>;

  HttpVideoOut();
  virtual ~HttpVideoOut() override;

  virtual void Write(Frame::Ptr frame) override;
  virtual bool IsStopped() const override;

  static Ptr Create(Options& /*opts*/, Io& /*io*/) { return std::make_shared<HttpVideoOut>(); }

 private:
  Streamer streamer_;
  std::unique_ptr<std::thread> streamer_thread_;
  bool stopped_ = false;
};

class RestVideoOut : public VideoOut {
 public:
  using Ptr = std::shared_ptr<RestVideoOut>;

  RestVideoOut(Options& opts, Io& io);

  static Ptr Create(Options& opts, Io& io) { return std::make_shared<RestVideoOut>(opts, io); }

  virtual bool IsStopped() const { return false; }
  virtual void Write(Frame::Ptr);

 private:
  Io& io_;
  RestClient rest_client_;
};

class VideoOutFactory {
 public:
  using VideoOutMap = std::map<VideoOut::Type, std::function<VideoOut::Ptr(Options& opts, Io& io)>>;

  VideoOutFactory() = default;

  static VideoOut::Ptr Create(VideoOut::Type type, Options& opts, Io& io);

 private:
  inline static VideoOutMap kRegistry{
      {VideoOut::Type::WINDOW, GuiVideoOut::Create}, {VideoOut::Type::HTTP, HttpVideoOut::Create}, {VideoOut::Type::REST, RestVideoOut::Create}};
};

}  // namespace omg

#endif  // OM_VIDEOOUTPUT_H
