#ifndef OM_FRAME_H
#define OM_FRAME_H

#include <memory>
#include <opencv2/opencv.hpp>

#include "dnn/face.h"

namespace omv {

struct Frame {
  using Ptr = std::shared_ptr<Frame>;

  Frame(int h, int w);

  void DisplayFaceBoxes();
  void DisplayFaceStats();
  void Rotate(uint16_t);

  cv::Mat m;
  std::vector<Face::Ptr> faces;
  std::vector<cv::Rect> face_boxes;

  double ts = 0;
  double te = 0;
  double fps = 0;
};
}  // namespace omg

#endif  // OM_FRAME_H
