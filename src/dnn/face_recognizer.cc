#include "face_recognizer.h"

#include "fmt/format.h"

#include "utils/logger.h"

namespace omv {

FaceRecognizer::FaceRecognizer(Options::Ptr opts) : BaseCNN{opts} {
  SetModelName(opts_["recognition"]);
}

void FaceRecognizer::GetResults(std::vector<float>& output) {
  iie::StatusCode rc = req_->Wait(iie::IInferRequest::WaitMode::RESULT_READY);

  if (rc == iie::StatusCode::OK) {
    const float* out = req_->GetBlob(output_name_)->buffer().as<float*>();

    std::vector<float> tmp(&out[0], &out[256]);
    output.assign(std::begin(tmp), std::end(tmp));
  } else
    LOG(ERROR) << "net get_result failed!";
}

void FaceRecognizer::GetResults(Face::Ptr face) {
  iie::StatusCode rc = req_->Wait(iie::IInferRequest::WaitMode::RESULT_READY);

  if (rc == iie::StatusCode::OK) {
    const float* out = req_->GetBlob(output_name_)->buffer().as<float*>();

    face->embeddings.assign(out, out + 256);
  } else
    LOG(ERROR) << "net get_result failed!";
}

float FaceRecognizer::ComputeReidDistance(const cv::Mat& descr1, const cv::Mat& descr2) {
  float xy = static_cast<float>(descr1.dot(descr2));
  float xx = static_cast<float>(descr1.dot(descr1));
  float yy = static_cast<float>(descr2.dot(descr2));
  float norm = sqrt(xx * yy) + 1e-6f;
  return 1.0f - xy / norm;
}

}  // namespace om
