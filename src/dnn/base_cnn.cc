#include "base_cnn.h"

#include "utils/logger.h"

#include "fmt/format.h"

namespace omv {

BaseCNN::BaseCNN(Options::Ptr opts) : opts_{*opts} {
  req_ = nullptr;
  input_blob_ = nullptr;
}

void BaseCNN::LoadIee(iie::Core& iie_core_) {
  const iie::Version* version = iie::GetInferenceEngineVersion();

  LOG(INFO) << "loaded Intel IE version: " << version->apiVersion.major << '.' << version->apiVersion.minor;
  LOG(INFO) << "loaded Intel IE plugin: " << iie_core_.GetVersions(IIE_DEVICE).at(IIE_DEVICE).description;

#if (!defined SMART_CAMERA_USE_MYRIAD && !defined SMART_CAMERA_USE_FPGA)
  iie_core_.AddExtension(std::make_shared<iie::Extensions::Cpu::CpuExtensions>(), IIE_DEVICE);
  LOG(INFO) << "loaded Intel " << IIE_DEVICE << " extensions";
#endif
}

void BaseCNN::Init(iie::Core& iie_core) {
  net_ = iie_core.LoadNetwork(ReadNetwork(), IIE_DEVICE, {});

  ShowInfo();
}

iie::CNNNetwork BaseCNN::ReadNetwork() {
  iie::CNNNetReader net_reader;
  std::string models_path = opts_["models-path"];

  net_reader.ReadNetwork(fmt::format("{}/{}/{}.xml", models_path, model_name_, model_name_));
  net_reader.ReadWeights(fmt::format("{}/{}/{}.bin", models_path, model_name_, model_name_));
  net_reader.getNetwork().setBatchSize(1);

  const auto& input_info = net_reader.getNetwork().getInputsInfo();
  const auto& output_info = net_reader.getNetwork().getOutputsInfo();

  input_name_ = input_info.begin()->first;
  output_name_ = output_info.begin()->first;

  for (auto& [name, layer] : input_info) {
    layer->getPreProcess().setResizeAlgorithm(iie::RESIZE_BILINEAR);
    layer->setPrecision(iie::Precision::U8);
  }

  for (auto& [name, layer] : output_info) {
    layer->setPrecision(iie::Precision::FP32);
  }

  return net_reader.getNetwork();
}

void BaseCNN::ShowInfo() {
  try {
    req_ = net_.CreateInferRequestPtr();
    input_blob_ = req_->GetBlob(input_name_);

    const auto blob_dims = input_blob_->getTensorDesc().getDims();

    input_width_ = blob_dims[3];
    input_height_ = blob_dims[2];
    input_channels_ = blob_dims[1];

    LOG(INFO) << model_name_ << " net dims (CxHxW): "  //
              << input_channels_                       //
              << 'x' << input_width_                   //
              << 'x' << input_height_;

    input_blob_.reset();
    req_.reset();

  } catch (std::exception& e) {
    LOG(ERROR) << "dummy inference failed: " << e.what();
    throw e;
  }
}

void BaseCNN::MakeAsyncRequest(const cv::Mat& frame) {
  req_ = net_.CreateInferRequestPtr();
  input_blob_ = WrapMat2Blob(frame);

  req_->SetBlob(input_name_, input_blob_);
  req_->StartAsync();
}

void BaseCNN::SetModelName(const std::string& model_name) {
  model_name_ = model_name;
}

iie::Blob::Ptr BaseCNN::WrapMat2Blob(const cv::Mat& mat) {
  size_t channels = static_cast<size_t>(mat.channels());
  size_t height = static_cast<size_t>(mat.size().height);
  size_t width = static_cast<size_t>(mat.size().width);

  size_t stride_h = mat.step.buf[0];
  size_t stride_w = mat.step.buf[1];

  bool is_dense = stride_w == channels && stride_h == channels * width;

  if (!is_dense)
    THROW_IE_EXCEPTION << "Doesn't support conversion from not dense cv::Mat";

  iie::TensorDesc t_desc(iie::Precision::U8, {1, channels, height, width}, iie::Layout::NHWC);

  return iie::make_shared_blob<uint8_t>(t_desc, mat.data);
}

}  // namespace omv
