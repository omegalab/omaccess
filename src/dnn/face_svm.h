#ifndef OM_FACESVM_H
#define OM_FACESVM_H

#include <map>

#include <opencv2/ml.hpp>

#include "face.h"
#include "omproto/store.pb.h"
#include "utils/options.h"

namespace omv {

class FaceSVM {
 public:
  using KernelsMap = std::map<std::string, cv::ml::SVM::KernelTypes>;
  using FeaturesMap = std::multimap<int, std::vector<float>>;

  FaceSVM(Options::Ptr);

  void Init();
  void Load();
  void Save();

  void Train(std::vector<std::string>&, FeaturesMap& train_data);
  void Predict(Face::Ptr, std::vector<float>&);
  void Predict(Face::Ptr);

  friend std::ostream& operator<<(std::ostream& os, const FaceSVM& F) {
    return os << "kernel: " << F.face_svm_->getKernelType()  //
              << "; c: " << F.face_svm_->getC()              //
              << "; gamma: " << F.face_svm_->getGamma()      //
              << "; degree: " << F.face_svm_->getDegree();
  }

 private:
  const KernelsMap kKernels{{"linear", cv::ml::SVM::LINEAR},    //
                            {"poly", cv::ml::SVM::POLY},        //
                            {"rbf", cv::ml::SVM::RBF},          //
                            {"sigmoid", cv::ml::SVM::SIGMOID},  //
                            {"chi2", cv::ml::SVM::CHI2},        //
                            {"inter", cv::ml::SVM::INTER}};
  Options& opts_;
  cv::Ptr<cv::ml::SVM> face_svm_;

  std::vector<std::string> labels_;
  omproto::Labels labels_proto_;
};

}  // namespace om

#endif  // OM_FACESVM_H
