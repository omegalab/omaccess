#include "face_aligner.h"

#include <cmath>

#include "utils/logger.h"

#include "fmt/format.h"

namespace omv {

static const float h = 112.;
static const float w = 96.;
static const float ref_landmarks_normalized[] = {30.2946f / w, 51.6963f / h, 65.5318f / w, 51.5014f / h, 48.0252f / w,
                                                 71.7366f / h, 33.5493f / w, 92.3655f / h, 62.7299f / w, 92.2041f / h};

FaceAligner::FaceAligner(Options::Ptr opts) : BaseCNN{opts} {
  SetModelName(opts_["alignment"]);
}

void FaceAligner::GetResults(Face::Ptr face) {
  iie::StatusCode rc = req_->Wait(iie::IInferRequest::WaitMode::RESULT_READY);

  if (rc == iie::StatusCode::OK)
    face->landmarks = cv::Mat(5, 2, CV_32F, req_->GetBlob(output_name_)->buffer());

  else
    LOG(ERROR) << "net get_result failed!";
}

void FaceAligner::AlignFace(Face::Ptr face) {
  cv::Mat ref_landmarks = cv::Mat(5, 2, CV_32F);

  for (int i = 0; i < ref_landmarks.rows; i++) {
    ref_landmarks.at<float>(i, 0) = ref_landmarks_normalized[2 * i] * face->m.cols;
    ref_landmarks.at<float>(i, 1) = ref_landmarks_normalized[2 * i + 1] * face->m.rows;
    face->landmarks.at<float>(i, 0) *= face->m.cols;
    face->landmarks.at<float>(i, 1) *= face->m.rows;
  }

  cv::Mat m = Transform(ref_landmarks, face->landmarks);
  cv::Size2f face_size = face->m.size();

  if (opts_["align-margin"]) {
    float margin = opts_["face-margin"];
    face_size.width *= margin;
    face_size.height *= margin;
  }

  cv::warpAffine(face->m, face->m, m, face_size, cv::WARP_INVERSE_MAP);
}

cv::Mat FaceAligner::Transform(cv::Mat& src, cv::Mat& dst) {
  cv::Mat col_mean_src;
  cv::reduce(src, col_mean_src, 0, cv::REDUCE_AVG);

  for (int i = 0; i < src.rows; i++) {
    src.row(i) -= col_mean_src;
  }

  cv::Mat col_mean_dst;
  cv::reduce(dst, col_mean_dst, 0, cv::REDUCE_AVG);

  for (int i = 0; i < dst.rows; i++) {
    dst.row(i) -= col_mean_dst;
  }

  cv::Scalar mean, dev_src, dev_dst;

  cv::meanStdDev(src, mean, dev_src);
  dev_src(0) = std::max(static_cast<double>(std::numeric_limits<float>::epsilon()), dev_src(0));
  src /= dev_src(0);

  cv::meanStdDev(dst, mean, dev_dst);
  dev_dst(0) = std::max(static_cast<double>(std::numeric_limits<float>::epsilon()), dev_dst(0));
  dst /= dev_dst(0);

  cv::Mat w, u, vt;
  cv::SVD::compute((src).t() * (dst), w, u, vt);
  cv::Mat r = (u * vt).t();
  cv::Mat m(2, 3, CV_32F);

  m.colRange(0, 2) = r * (dev_dst(0) / dev_src(0));
  m.col(2) = (col_mean_dst.t() - m.colRange(0, 2) * col_mean_src.t());

  return m;
}

}  // namespace om
