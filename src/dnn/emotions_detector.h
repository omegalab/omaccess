#ifndef OM_EMTIONSDETECTOR_H
#define OM_EMTIONSDETECTOR_H

#include "base_cnn.h"
#include "face.h"

namespace omv {

class EmotionsDetector : public BaseCNN {
 public:
  using Ptr = std::unique_ptr<EmotionsDetector>;
  EmotionsDetector(Options::Ptr opts);

  void GetResults(Face::Ptr);
};

}  // namespace omg

#endif  // OM_EMTIONSDETECTOR_H
