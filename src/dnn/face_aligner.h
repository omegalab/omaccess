#ifndef OM_FACEALIGNER_H
#define OM_FACEALIGNER_H

#include "base_cnn.h"
#include "face.h"

namespace omv {

class FaceAligner : public BaseCNN {
 public:
  using Ptr = std::unique_ptr<FaceAligner>;
  FaceAligner(Options::Ptr opts);

  void GetResults(Face::Ptr);
  void AlignFace(Face::Ptr);

 private:
  cv::Mat input_mat_;
  cv::Mat Transform(cv::Mat& src, cv::Mat& dst);
};

}  // namespace omg

#endif  // OM_FACEALIGNER_H
