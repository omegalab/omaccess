#include "face_svm.h"

#include <filesystem>
#include <fstream>

#include "fmt/format.h"

#include "utils/logger.h"

namespace fs = std::filesystem;

namespace omv {

FaceSVM::FaceSVM(Options::Ptr opts) : opts_{*opts} {
  GOOGLE_PROTOBUF_VERIFY_VERSION;
}

void FaceSVM::Init() {
  std::string kernel_type = opts_["svm-kernel"];

  if (!kKernels.count(kernel_type))
    throw std::runtime_error(fmt::format("unknown kernel type: {}", kernel_type));

  face_svm_ = cv::ml::SVM::create();
  face_svm_->setType(cv::ml::SVM::C_SVC);
  face_svm_->setKernel(kKernels.at(kernel_type));
  face_svm_->setGamma(opts_["svm-gamma"]);
  face_svm_->setDegree(opts_["svm-degree"]);
  face_svm_->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, 100, 1e-6));

  LOG(INFO) << "created SVM: " << *this;
}

void FaceSVM::Load() {
  fs::path train_data_path;
  std::vector<fs::path> paths;

  if (opts_.Has("svm-data"))
    train_data_path = fs::path(opts_.Get<std::string>("svm-data"));
  else
    train_data_path = fs::current_path();

  fs::path proto_file_path = train_data_path / "face_labels.dat";
  fs::path svm_file_path = train_data_path / "face_data.yml";

  std::ifstream input(proto_file_path, std::ios::in | std::ios::binary);

  if (!labels_proto_.ParseFromIstream(&input))
    throw std::runtime_error(fmt::format("failed to read face registry from {}", proto_file_path.string()));

  face_svm_ = face_svm_->load(svm_file_path.string());

  for (auto& label : labels_proto_.names())
    labels_.push_back(label);

  LOG(INFO) << "loaded SVM: " << *this;
}

void FaceSVM::Save() {
  fs::path train_data_path;

  if (opts_.Has("output-path"))
    train_data_path = fs::path(opts_.Get<std::string>("output-path"));
  else
    train_data_path = fs::current_path();

  fs::path proto_file_path = train_data_path / "face_labels.dat";
  fs::path svm_file_path = train_data_path / "face_data.yml";

  std::ofstream fs(proto_file_path, std::ios::trunc | std::ios::binary);

  if (!labels_proto_.SerializeToOstream(&fs)) {
    throw std::runtime_error(fmt::format("failed to save face registry to {}", proto_file_path.string()));
  }

  face_svm_->save(svm_file_path.string());
}

void FaceSVM::Train(std::vector<std::string>& labels, FeaturesMap& train_data) {
  cv::Mat labels_mat = cv::Mat::zeros(0, static_cast<int>(train_data.size()), CV_32SC1);
  cv::Mat train_mat = cv::Mat::zeros(0, 256, CV_32F);
  cv::Mat test_mat;

  std::vector<int> label_ids;
  bool enable_test = opts_["test-data"];

  for (auto& [id, features] : train_data) {
    train_mat.push_back(cv::Mat(1, 256, CV_32F, features.data()));
    labels_mat.push_back(id);
  }

  auto td = cv::ml::TrainData::create(train_mat, cv::ml::ROW_SAMPLE, labels_mat);

  if (enable_test)
    td->setTrainTestSplitRatio(opts_["test-ratio"]);

  if (!face_svm_->trainAuto(td))
    throw(std::runtime_error(fmt::format("failed training SVM model")));

  int error = -1;

  if (enable_test)
    error = static_cast<int>(face_svm_->calcError(td, false, test_mat) * 100);

  *labels_proto_.mutable_names() = {std::begin(labels), std::end(labels)};
  labels_.assign(std::begin(labels), std::end(labels));

  LOG(INFO) << fs::path(opts_.Get<std::string>("train-path")).filename()  //
            << " (error: " << error << "%) "                              //
            << labels_proto_.ShortDebugString();
}

void FaceSVM::Predict(Face::Ptr face, std::vector<float>& input) {
  cv::Mat output_mat = cv::Mat::zeros(1, 1, CV_32S);
  cv::Mat input_mat(1, 256, CV_32F, input.data());

  face_svm_->predict(input_mat, output_mat);
  size_t id = static_cast<size_t>(output_mat.at<float>(0, 0));

  face->id = id;
  face->name = labels_[id];
}

void FaceSVM::Predict(Face::Ptr face) {
  cv::Mat output_mat = cv::Mat::zeros(1, 1, CV_32S);
  cv::Mat input_mat(1, 256, CV_32F, face->embeddings.data());

  face_svm_->predict(input_mat, output_mat);
  size_t id = static_cast<size_t>(output_mat.at<float>(0, 0));

  face->id = id;
  face->name = labels_[id];
}

}  // namespace om
