#ifndef OM_FACEDETECTOR_H
#define OM_FACEDETECTOR_H

#include "base_cnn.h"

namespace omv {

class FaceDetector : public BaseCNN {
 public:
  using Ptr = std::unique_ptr<FaceDetector>;
  FaceDetector(Options::Ptr opts);

  void GetResults(std::vector<cv::Rect>& output, int h, int w);

 private:
  size_t max_faces_;

  static cv::Rect AddMarginToRect(const cv::Rect& r, float margin);
  static cv::Rect TruncateToValidRect(const cv::Rect& rect, const cv::Size& size);
};

}  // namespace omg

#endif  // OM_FACEDETECTOR_H
