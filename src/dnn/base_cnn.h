#ifndef OM_BASECNN_H
#define OM_BASECNN_H

#include <inference_engine.hpp>
#include <opencv2/opencv.hpp>

#include "utils/helper.h"
#include "utils/options.h"

#if defined SMART_CAMERA_USE_MYRIAD
#define IIE_DEVICE "MYRIAD"
#elif defined SMART_CAMERA_USE_FPGA
#define IIE_DEVICE "FPGA"
#else
#include <ext_list.hpp>
#define IIE_DEVICE "CPU"
#endif

namespace iie = InferenceEngine;

namespace omv {

class BaseCNN {
 public:
  BaseCNN(Options::Ptr opts);
  virtual ~BaseCNN() = default;

  static void LoadIee(iie::Core&);
  void Init(iie::Core& ie_core);

  iie::CNNNetwork ReadNetwork();
  static iie::Blob::Ptr WrapMat2Blob(const cv::Mat& mat);

  void MakeAsyncRequest(const cv::Mat& frame);

 protected:
  Options& opts_;

  iie::ExecutableNetwork net_;
  iie::InferRequest::Ptr req_;
  iie::Blob::Ptr input_blob_;

  std::string input_name_;
  std::string output_name_;

  size_t input_width_;
  size_t input_height_;
  size_t input_channels_;

  void SetModelName(const std::string& model_name);

 private:
  std::string model_name_;

  void ShowInfo();
};

}  // namespace omv

#endif  // OM_BASECNN_H
