﻿#include "face_registry.h"

#include <future>

#include <opencv2/opencv.hpp>

#include "fmt/format.h"

#include "utils/helper.h"
#include "utils/json.h"
#include "utils/logger.h"

namespace omv {

FaceRegistry::FaceRegistry(Options::Ptr opts, Io& io) : opts_{*opts}, io_(io) {
  reset_timer_ = std::make_unique<Timer>(io);
}

void FaceRegistry::Init() {
  rest_client_.Init(opts_);
  reset_timer_->AsyncWait(
      [&](size_t) {
        if (!data_.empty()) {
          data_.clear();
        }
      },
      opts_["reset-timer"]);
}

void FaceRegistry::RegisterFace(Face::Ptr face, bool report) {
  if (!data_.count(face->id) && report)
    SendReport(face);

  data_[face->id] = face;

  /* TODO: remove this shit
 unknownn_.push_back(face->id);

 size_t threshold = opts_["threshold"];
 float level = opts_["confidence"];

 if (unknownn_.size() < threshold)
   return;

 using Range = std::pair<size_t, size_t>;

 std::vector<Range> ranges;
 std::sort(std::begin(unknownn_), std::end(unknownn_));

 auto i = std::begin(unknownn_);

 while (i != std::end(unknownn_)) {
   auto [rs, re] = std::equal_range(i, std::end(unknownn_), *i);

   ranges.emplace_back(std::pair{*i, (rs - re)});
   i = re;
 }

 std::sort(ranges.begin(), ranges.end(), [](Range& lhs, Range& rhs) { return (rhs.second > lhs.second); });

 const auto [id, count] = ranges[0];

 if (id != 0 && count > level * threshold) {
   face->id = id;
   face->name = labels_[id];
   faces_ids_.insert(id);
   faces_.push_back(face);
   SendReport(face);
 }

 unknownn_.clear();
 */
}

void FaceRegistry::SendReport(Face::Ptr face) {
  io_.Post([&, face]() {
    Json json;

    json.Set("camera", 1);
    json.Set("employee", face->id);

    LOG(DEBUG) << json;
    rest_client_.HttpPost("visit/add", std::move(json), [](const std::string& res) { LOG(INFO) << res; });
  });
}

void FaceRegistry::Display(Frame::Ptr frame) {
  if (data_.empty())
    return;

  int text_y = 30;

  auto put_label = [&](const std::string& label) {
    auto label_size = cv::getTextSize(label, cv::FONT_HERSHEY_DUPLEX, 0.6, 1, nullptr);
    cv::putText(frame->m, label, cv::Point(10, text_y), cv::FONT_HERSHEY_DUPLEX, 0.6, cv::Scalar(0, 255, 0), 1);

    text_y += label_size.height + label_size.height / 2;
  };

  for (auto& [id, face] : data_)
    put_label(fmt::format("id: {:#02} name: {}; a: {}; g: {}; e: {}",  //
                          id, face->name, face->age, face->gender, face->emotion));
}

}  // namespace omv
