#include "face_detector.h"
#include <fstream>
#include <memory>

#include "fmt/format.h"

#include "utils/logger.h"

namespace omv {

FaceDetector::FaceDetector(Options::Ptr opts) : BaseCNN{opts} {
  SetModelName(opts_["detection"]);
}

void FaceDetector::GetResults(std::vector<cv::Rect>& output, int height, int width) {
  iie::StatusCode rc = req_->Wait(iie::IInferRequest::WaitMode::RESULT_READY);

  if (rc == iie::StatusCode::OK) {
    auto blob = req_->GetBlob(output_name_);
    auto dims = blob->getTensorDesc().getDims();

    const float* predictions = blob->buffer().as<float*>();

    float margin = opts_["face-margin"];

    for (size_t i = 0; i < dims[2]; i++) {
      //    float id = predictions[i * 7];
      //    float cls = predictions[i * 7 + 1];
      float score = predictions[i * 7 + 2];

      if (score > 0.7f) {
        int x1 = static_cast<int>(predictions[i * 7 + 3] * width);
        int y1 = static_cast<int>(predictions[i * 7 + 4] * height);
        int x2 = static_cast<int>((predictions[i * 7 + 5] - predictions[i * 7 + 3]) * width);
        int y2 = static_cast<int>((predictions[i * 7 + 6] - predictions[i * 7 + 4]) * height);

        auto rect = TruncateToValidRect(AddMarginToRect(cv::Rect{x1, y1, x2, y2}, margin),
                                        cv::Size(static_cast<int>(width), static_cast<int>(height)));

        output.emplace_back(rect);
      }
    }
  }

  else {
    LOG(ERROR) << "net get_result failed!";
  }
}

cv::Rect FaceDetector::AddMarginToRect(const cv::Rect& r, float margin) {
  cv::Point2f tl = r.tl();
  cv::Point2f br = r.br();
  cv::Point2f c = (tl * 0.5f) + (br * 0.5f);
  cv::Point2f diff = c - tl;
  cv::Point2f new_diff{diff.x * margin, diff.y * margin};
  cv::Point2f new_tl = c - new_diff;
  cv::Point2f new_br = c + new_diff;

  cv::Point new_tl_int{static_cast<int>(std::floor(new_tl.x)), static_cast<int>(std::floor(new_tl.y))};
  cv::Point new_br_int{static_cast<int>(std::ceil(new_br.x)), static_cast<int>(std::ceil(new_br.y))};

  return cv::Rect(new_tl_int, new_br_int);
}

cv::Rect FaceDetector::TruncateToValidRect(const cv::Rect& rect, const cv::Size& size) {
  auto tl = rect.tl(), br = rect.br();
  tl.x = std::max(0, std::min(size.width - 1, tl.x));
  tl.y = std::max(0, std::min(size.height - 1, tl.y));
  br.x = std::max(0, std::min(size.width, br.x));
  br.y = std::max(0, std::min(size.height, br.y));
  int w = std::max(0, br.x - tl.x);
  int h = std::max(0, br.y - tl.y);
  return cv::Rect(tl.x, tl.y, w, h);
}

}  // namespace om
