#ifndef OM_FACERECOGNIZER_H
#define OM_FACERECOGNIZER_H

#include "base_cnn.h"
#include "face.h"

namespace omv {

class FaceRecognizer : public BaseCNN {
 public:
  using Ptr = std::unique_ptr<FaceRecognizer>;
  FaceRecognizer(Options::Ptr opts);

  void GetResults(std::vector<float>& output);
  void GetResults(Face::Ptr);

  static float ComputeReidDistance(const cv::Mat& descr1, const cv::Mat& descr2);
};

}  // namespace omg

#endif  // OM_FACERECOGNIZER_H
