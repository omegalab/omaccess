#ifndef OM_FACEREGISTRY_H
#define OM_FACEREGISTRY_H

#include "face.h"

#include "aio/io.h"
#include "aio/timer.h"
#include "camera/frame.h"
#include "utils/options.h"
#include "utils/rest_client.h"

namespace omv {

class FaceRegistry {
 public:
  using KernelsMap = std::map<std::string, cv::ml::SVM::KernelTypes>;
  using FeaturesMap = std::multimap<int, std::vector<float>>;

  FaceRegistry(Options::Ptr, Io& io);

  void Init();

  void RegisterFace(Face::Ptr, bool report = true);
  void SendReport(Face::Ptr);
  void Display(Frame::Ptr);

 private:
  Options& opts_;
  Io& io_;

  RestClient rest_client_;
  Timer::Ptr reset_timer_;

  std::map<size_t, Face::Ptr> data_;

  //  struct Data {
  //    std::vector<Face::Ptr> faces;
  //    std::set<size_t> ids;

  //    void Add(Face::Ptr face);
  //    bool IsEmpty() const;
  //    void Clear();

  //  } data_;
};

}  // namespace omg

#endif  // OM_FACEREGISTRY_H
