#include "emotions_detector.h"

#include "fmt/format.h"

#include "utils/logger.h"

namespace omv {

EmotionsDetector::EmotionsDetector(Options::Ptr opts) : BaseCNN{opts} {
  SetModelName(opts_["emotions"]);
}

void EmotionsDetector::GetResults(Face::Ptr face) {
  iie::StatusCode rc = req_->Wait(iie::IInferRequest::WaitMode::RESULT_READY);

  if (rc == iie::StatusCode::OK) {
    float* blob = req_->GetBlob("prob_emotion")->buffer().as<float*>();
    std::vector<float> emotions(blob, blob + 5);

    auto it = std::max_element(emotions.begin(), emotions.end());
    int emotion = static_cast<int>(std::distance(emotions.begin(), it));

    face->emotion = kEmotions.at(emotion);
  }
}
}  // namespace om
