#ifndef OM_AGEGENDERDETECTOR_H
#define OM_AGEGENDERDETECTOR_H

#include "base_cnn.h"
#include "face.h"

namespace omv {

class AgeGenderDetector : public BaseCNN {
 public:
  using Ptr = std::unique_ptr<AgeGenderDetector>;
  AgeGenderDetector(Options::Ptr opts);

  void GetResults(Face::Ptr);
};

}  // namespace omg

#endif  // OM_AGEGENDERDETECTOR_H
