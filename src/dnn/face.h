#ifndef OM_FACE_H
#define OM_FACE_H

#include <memory>

#include <opencv2/opencv.hpp>

namespace omv {

static const std::map<int, std::string> kGenders{{0, "Female"}, {1, "Male"}};
static const std::map<int, std::string> kEmotions{
    {0, "Neutral"}, {1, "Happy"}, {2, "Sad"}, {3, "Surprise"}, {4, "Angry"}};

struct Face {
  using Ptr = std::shared_ptr<Face>;

  Face(cv::Mat& image, cv::Rect& rect) : r{rect}, landmarks{cv::Mat::zeros(5, 2, CV_32F)}, embeddings{256, 0} {
    m = image(rect).clone();
  }

  cv::Mat m;
  cv::Rect r;

  cv::Mat landmarks;
  std::vector<float> embeddings;

  size_t id = 0;
  std::string name;
  int age;
  std::string gender;
  std::string emotion;
};

}  // namespace om

#endif  // OM_FACE_H
