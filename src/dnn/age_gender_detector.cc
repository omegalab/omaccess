#include "age_gender_detector.h"

#include <algorithm>
#include <cmath>

#include "fmt/format.h"

#include "utils/logger.h"

namespace omv {

AgeGenderDetector::AgeGenderDetector(Options::Ptr opts) : BaseCNN{opts} {
  SetModelName(opts_["age-gender"]);
}

void AgeGenderDetector::GetResults(Face::Ptr face) {
  iie::StatusCode rc = req_->Wait(iie::IInferRequest::WaitMode::RESULT_READY);

  if (rc == iie::StatusCode::OK) {
    float* blob = req_->GetBlob("prob")->buffer().as<float*>();
    std::vector<float> genders(blob, blob + 2);

    auto it = std::max_element(genders.begin(), genders.end());
    int gender = static_cast<int>(std::distance(genders.begin(), it));

    face->age = static_cast<int>(std::round(*req_->GetBlob("age_conv3")->buffer().as<float*>() * 100));
    face->gender = kGenders.at(gender);
  }
}

}  // namespace om
