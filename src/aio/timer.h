#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <memory>

#include <fcntl.h>
#include <sys/timerfd.h>

#include "handler.h"
#include "io.h"

namespace omv {

class Timer {
 public:
  using Ptr = std::unique_ptr<Timer>;

  Timer(Io& io);
  ~Timer();

  void AsyncWait(TimerHandler::Sig&& func, size_t delay = 0);

 private:
  Io& io_;
  int fd_;

  void SetupTimer(const std::chrono::seconds& delay);
};
}  // namespace om

#endif  // TIMER_H
