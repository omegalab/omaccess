#ifndef ACCEPTOR_H
#define ACCEPTOR_H

#include <netinet/ip.h>
#include <sys/socket.h>

#include "common/options.h"
#include "io/io.h"
#include "net/tcp_socket.h"

class Acceptor {
 public:
  //  Acceptor(Io& io, const Options&);
  Acceptor(Io& io, uint16_t port);
  ~Acceptor();

  void Bind();
  void Listen();

  void AsyncAccept(AcceptHandler::Sig&&);

 private:
  Io& io_;
  TcpSocket socket_;

  sockaddr_in addr_;
};

#endif  // ACCEPTOR_H
