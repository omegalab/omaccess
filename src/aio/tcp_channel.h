#pragma once

#include <arpa/inet.h>

#include "common/logger.h"
#include "common/options.h"
#include "io/io.h"
#include "net/acceptor.h"
#include "net/base_channel.h"
#include "net/base_parser.h"
#include "net/tcp_session.h"

template <typename ParserType>
class TcpChannel : public Channel {
 public:
  using Ptr = std::unique_ptr<TcpChannel>;

  TcpChannel(Io& io, uint16_t port) : io_(io), acceptor_(io, port) {
    acceptor_.Bind();
    acceptor_.Listen();
  }

  void Start() override {
    acceptor_.AsyncAccept([&](int socket_fd) {
      struct sockaddr_in addr;
      try {
        socklen_t addr_size = sizeof(struct sockaddr_in);

        if (!getpeername(socket_fd, (struct sockaddr*)&addr, &addr_size))
          LOG(INFO) << "new connection from " << inet_ntoa(addr.sin_addr) << ':'
                    << htons(addr.sin_port);

        auto socket = std::make_shared<TcpSocket>(io_, socket_fd);

        //        TcpSocket socket(io_, socket_fd);
        socket->SetNonBlocking();

        auto parser = std::make_shared<ParserType>();
        //        auto session =
        //        std::make_shared<TcpSession>(std::move(socket));
        auto session = std::make_shared<TcpSession>(socket);
        session->Start(parser);
        sessions_.push_back(session);
      } catch (SessionClosed& ex) {
        LOG(INFO) << "connection closed " << inet_ntoa(addr.sin_addr) << ':'
                  << htons(addr.sin_port);
      }
    });
  }

 private:
  Io& io_;
  Acceptor acceptor_;
  std::vector<TcpSession::Ptr> sessions_;
};
