#include "queue.h"

#include <iostream>
#include <stdexcept>

#include "utils/logger.h"

namespace omv {

Queue::Queue() : stopped_(false) {}

Queue::~Queue() {
  if (!stopped_)
    Stop();

  for (auto& thread : threads_) {
    if (thread.joinable())
      thread.join();
  }
}

void Queue::Add(Task::Future&& future) {
  {
    std::unique_lock<std::mutex> lock(lock_);

    if (queue_.size() > 40) {
      LOG(WARN) << "max queue: " << queue_.size();
      queue_.pop();
    }

    queue_.emplace(std::make_unique<Task>(std::move(future)));
  }
  cv_.notify_one();
}

void Queue::Start(uint32_t num_cpus) {
  if (num_cpus < 1) {
    num_cpus = std::thread::hardware_concurrency();

    if (num_cpus <= 1)
      num_cpus = 1;
    else
      num_cpus /= 2;
  }

  for (size_t i = 0; i < num_cpus; i++) {
    threads_.emplace_back(std::thread(&Queue::Wait, this));
  }

  LOG(DEBUG) << "queue started (workers count: " << threads_.size() << ')';
}

void Queue::Stop() {
  stopped_ = true;
  cv_.notify_all();

  LOG(DEBUG) << "queue stopped";
}

void Queue::Wait() {
  std::unique_lock<std::mutex> lock(lock_);

  while (!stopped_) {
    cv_.wait(lock, [this] { return (!queue_.empty() || stopped_); });

    if (stopped_)
      break;

    if (!queue_.empty()) {
      auto task = std::move(queue_.front());
      queue_.pop();

      lock.unlock();
      task->Run();
      lock.lock();
    }
  }
}

}  // namespace omg
