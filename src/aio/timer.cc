#include "timer.h"

#include <errno.h>

namespace omv {

Timer::Timer(Io& io) : io_(io) {
  fd_ = timerfd_create(CLOCK_MONOTONIC, 0);

  if (fd_ < 0)
    throw std::runtime_error("create timer object failed");
}

Timer::~Timer() {
  close(fd_);
}

void Timer::AsyncWait(TimerHandler::Sig&& func, size_t delay) {
  SetupTimer(std::chrono::seconds(delay));

  auto handler = std::make_shared<TimerHandler>(std::move(func));
  handler->e.data.fd = fd_;
  handler->e.events = EPOLLIN | EPOLLET;

  io_.Register(handler);
}

void Timer::SetupTimer(const std::chrono::seconds& delay) {
  itimerspec ts;
  ts.it_interval.tv_sec = delay.count();
  ts.it_interval.tv_nsec = 0;
  ts.it_value.tv_sec = delay.count();
  ts.it_value.tv_nsec = 0;

  if (timerfd_settime(fd_, 0, &ts, nullptr) < 0)
    throw std::runtime_error("set timer time failed: errno " + std::to_string(errno));
}

}  // namespace om
