#include "net/acceptor.h"

#include <arpa/inet.h>
#include <netinet/in.h>

Acceptor::Acceptor(Io& io, uint16_t port) : io_(io), socket_(io) {
  socket_.Open();
  socket_.SetNonBlocking();

  addr_.sin_family = AF_INET;
  addr_.sin_port = htons(port);
  addr_.sin_addr.s_addr = INADDR_ANY;

  //  if (inet_pton(AF_INET, opts.GetRelay().c_str(), &addr_.sin_addr) <= 0)
  //    throw std::runtime_error("bad address");
}

Acceptor::~Acceptor() {
  socket_.Close();
}

void Acceptor::Bind() {
  if (bind(socket_.Raw(), (sockaddr*)&addr_, sizeof(addr_)) < 0)
    throw std::runtime_error("bind failed");
}

void Acceptor::Listen() {
  if (listen(socket_.Raw(), 1024) < 0)
    throw std::runtime_error("listen failed");
}

void Acceptor::AsyncAccept(AcceptHandler::Sig&& f) {
  auto handler = std::make_shared<AcceptHandler>(std::move(f));

  handler->e.events = EPOLLIN | EPOLLET;
  handler->e.data.fd = socket_.Raw();

  io_.Register(handler);
}
