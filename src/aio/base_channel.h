#ifndef CHANNEL_H
#define CHANNEL_H

#include <memory>

#include "net/base_parser.h"

class Channel {
 public:
  using Ptr = std::unique_ptr<Channel>;

  Channel();
  virtual ~Channel();

  virtual void Start() {}
};

#endif  // CHANNEL_H
