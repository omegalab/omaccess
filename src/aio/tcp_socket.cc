#include "net/tcp_socket.h"

#include <arpa/inet.h>
#include <netinet/in.h>

void TcpSocket::Open() {
  fd_ = socket(AF_INET, SOCK_STREAM, 0);

  if (fd_ < 0)
    throw std::runtime_error("tcp socket open failed");

  int opts = -1;

  int err = setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opts,
                       sizeof(opts));

  if (err != 0)
    throw std::runtime_error("tcp socket setsockopt failed");
}

void TcpSocket::AsyncReadSome(TcpReadSomeHandler::Sig&& f, ssize_t len) const {
  auto handler = std::make_shared<TcpReadSomeHandler>(std::move(f), len);

  handler->e.events = EPOLLIN | EPOLLERR | EPOLLONESHOT;
  handler->e.data.fd = fd_;

  io_.Register(handler);
}

void TcpSocket::Send(const std::string& data) {
  send(fd_, data.data(), data.size(), 0);
}

void TcpSocket::Connect(const std::string& ip, uint16_t port) {
  sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr) <= 0)
    throw std::runtime_error("bad address");

  if (connect(fd_, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    throw std::runtime_error("connection failed");
}
