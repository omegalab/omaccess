#ifndef QUEUE_H
#define QUEUE_H

#include <atomic>
#include <condition_variable>
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <thread>

#include "task.h"

namespace omv {

class Queue {
 public:
  Queue();
  ~Queue();

  void Start(uint32_t num_cpus = 0);
  void Stop();

  void Add(Task::Future&&);

 private:
  std::mutex lock_;
  std::vector<std::thread> threads_;
  std::queue<Task::Ptr> queue_;
  std::condition_variable cv_;
  std::atomic_bool stopped_;

  void Wait();
};

}  // namespace omega

#endif  // QUEUE_H
