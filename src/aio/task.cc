#include "task.h"

namespace omv {

Task::Task(Task::Future&& future) : future_(std::move(future)) {}

void Task::Run() {
  future_.get();
}

}  // namespace om
