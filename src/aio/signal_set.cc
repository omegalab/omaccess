#include "signal_set.h"

#include <iostream>

namespace omv {

void SignalSet::AsyncWait(SignalsHandler::Sig&& f) {
  auto handler = std::make_shared<SignalsHandler>(std::move(f));

  handler->e.data.fd = fd_;
  handler->e.events = EPOLLIN | EPOLLET;

  io_.Register(handler);
}

}  // namespace om
