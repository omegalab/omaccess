#include "net/base_socket.h"

#include <fcntl.h>

Socket::Socket(Io& io) : io_(io) {}

Socket::Socket(Io& io, int fd) : io_(io), fd_(fd) {}

Socket::~Socket() {}

void Socket::SetNonBlocking() {
  int opts = fcntl(fd_, F_GETFL);

  if (opts < 0)
    throw std::runtime_error("socket fcntl(F_GETFL) failed");

  opts = (opts | O_NONBLOCK);

  if (fcntl(fd_, F_SETFL, opts) < 0)
    throw std::runtime_error("socket fcntl(F_SETFL) failed");
}
