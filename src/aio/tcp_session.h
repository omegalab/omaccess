#ifndef TCP_SESSION_H
#define TCP_SESSION_H

#include <memory>
#include <stdexcept>

#include "io/handler.h"
#include "io/io.h"
#include "net/base_parser.h"
#include "net/tcp_socket.h"

class SessionClosed : public std::runtime_error {
 public:
  explicit SessionClosed(const std::string& msg) : std::runtime_error(msg) {}
};

class TcpSession : public std::enable_shared_from_this<TcpSession> {
 public:
  using Ptr = std::shared_ptr<TcpSession>;

  //  TcpSession(TcpSocket&& socket);
  TcpSession(TcpSocket::Ptr socket) {
    socket_ = socket;
  }
  ~TcpSession();

  void Start(Parser::Ptr parser);
  void Send(const std::string& reply);

 private:
  //  TcpSocket socket_;
  TcpSocket::Ptr socket_;
  Parser::Ptr parser_;
};

#endif  // TCP_SESSION_H
