#ifndef TASK_H
#define TASK_H

#include <future>

namespace omv {

class Task {
 public:
  using Ptr = std::unique_ptr<Task>;
  using Future = std::future<void>;

  Task(Future&& future);

  void Run();

 private:
  std::future<void> future_;
};

}  // namespace om

#endif  // TASK_H
