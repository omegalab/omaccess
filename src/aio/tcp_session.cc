#include "net/tcp_session.h"

// TcpSession::TcpSession(TcpSocket&& socket) : socket_(std::move(socket)) {}

TcpSession::~TcpSession() {
  socket_->Close();
}

void TcpSession::Start(Parser::Ptr parser) {
  try {
    parser_ = parser;

    parser_->StartReader(socket_);
  } catch (SessionClosed& e) {
    LOG(WARN) << "cp" << e.what();
  }
}

void TcpSession::Send(const std::string& reply) {
  socket_->Send(reply);
}
