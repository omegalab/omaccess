#ifndef TCP_SOCKET_H
#define TCP_SOCKET_H

#include <memory>
#include "net/base_socket.h"

class TcpSocket : public Socket,
                  public std::enable_shared_from_this<TcpSocket> {
 public:
  using Socket::Socket;
  using Ptr = std::shared_ptr<TcpSocket>;

  void Open() override;

  void Connect(const std::string&, uint16_t);
  void AsyncReadSome(TcpReadSomeHandler::Sig&& f, ssize_t len) const;
  void Send(const std::string& data);
};

#endif  // TCP_SOCKET_H
