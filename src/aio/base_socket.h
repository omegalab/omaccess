#ifndef SOCKET_H
#define SOCKET_H

#include <sys/socket.h>
#include <memory>

#include "io/handler.h"
#include "io/io.h"

class Socket {
 public:
  using Ptr = std::shared_ptr<Socket>;

  Socket(Io& io);
  Socket(Io& io, int);
  virtual ~Socket();

  virtual void Open() = 0;
  int Raw() const {
    return fd_;
  }

  void Close() const {
    close(fd_);
    //    io_.Release(fd_);
  }

  void SetNonBlocking();

 protected:
  Io& io_;
  int fd_;
};

#endif  // SOCKET_H
