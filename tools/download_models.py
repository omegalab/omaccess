# -*- coding: utf-8 -*-

import asyncio
import os

import aiohttp
import aiofiles

CWD = os.getcwd()
URL = "https://download.01.org/opencv/2019/open_model_zoo/R3/20190905_163000_models_bin"

MODELS = ["face-detection-adas-0001",
          "landmarks-regression-retail-0009",
          "face-reidentification-retail-0095",
          "age-gender-recognition-retail-0013",
          "emotions-recognition-retail-0003"]


async def create_model_directory(model_name):
    model_directory = os.path.join(CWD, model_name)
    if not os.path.exists(model_directory):
        os.mkdir(model_directory)


async def download_url(model_name, url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                await save_file(resp, model_name, os.path.basename(url))
            else:
                print("error downloading model: %s" % (model_name,))


async def save_file(response, model_name, file_name):
    print("downloaded: %s/%s" % (model_name, file_name))
    file_path = os.path.join(CWD, model_name, file_name)
    async with aiofiles.open(file_path, "w+b") as fd:
        while True:
            chunk = await response.content.read(1024)
            if not chunk:
                break
            await fd.write(chunk)


async def download_model_files(model_name):
    for file in [f"FP16/{model_name}.{ext}" for ext in ("xml", "bin")]:
        file_url = f"{URL}/{model_name}/{file}"
        await download_url(model_name, file_url)


async def download():
    for model in MODELS:
        await create_model_directory(model)
        await download_model_files(model)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(download())
