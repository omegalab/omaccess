# -*- coding: utf-8 -*-

import os
import shutil
import cv2 as cv
import sys
from random import shuffle

CONFIDENCE = 0.7
SPLIT = sys.argv[1] if len(sys.argv) > 1 else None

# SPLIT = 50

INPUT = "omega.new"
TRAIN = f"omega.new.train.{SPLIT}" if SPLIT else "omega.new.train"
TEST = f"omega.new.test.{SPLIT}" if SPLIT else "omega.new.test"

LABELS = {"Unknown": "00"}


def find_face(net, image):
    frame = cv.imread(image)

    if frame is None:
        raise Exception(f'Image not found! {image}')

    blob = cv.dnn.blobFromImage(frame, size=(672, 384), ddepth=cv.CV_8U)
    net.setInput(blob)
    out = net.forward()
    detections = out.reshape(-1, 7)

    return (len(detections) > 0 and float(detections[0][2]) > CONFIDENCE)


if __name__ == "__main__":
    net = cv.dnn.readNet('/usr/local/omaccess/models/face-detection-adas-0001/face-detection-adas-0001.xml',
                         '/usr/local/omaccess/models/face-detection-adas-0001/face-detection-adas-0001.bin')

    net.setPreferableBackend(cv.dnn.DNN_BACKEND_INFERENCE_ENGINE)
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)

    inner_dirs = []

    for new_dir in [TRAIN, TEST]:
        if not os.path.exists(new_dir):
            os.mkdir(new_dir, mode=0o755)
        else:
            for item in os.listdir(new_dir):
                shutil.rmtree(os.path.join(new_dir, item))

    for dir_name in os.listdir(INPUT):
        if os.path.isdir(os.path.join(INPUT, dir_name)):
            inner_dirs.append(dir_name)

    inner_dirs.remove("Unknown")

    for idx, dir_name in enumerate(inner_dirs, start=1):
        LABELS.update({dir_name: f"{idx:02d}"})

    new_paths = []
    raw_data = []

    for k, v in LABELS.items():
        new_paths += [os.path.join(name, f"{v}.{k}") for name in (TRAIN, TEST)]
        raw_data += [os.path.join(INPUT, k)]

    for new_path in new_paths:
        if not os.path.exists(new_path):
            os.mkdir(new_path, mode=0o755)

    total_good = []
    total_bad = []

    for raw_path in raw_data:
        current_person = os.path.basename(raw_path)
        person_images = [photo_file for photo_file in os.listdir(raw_path)]

        shuffle(person_images)

        good_photos = []
        test_photos = []
        bad_photos = []

        for file in person_images:
            image_path = os.path.join(raw_path, file)

            if find_face(net, image_path):
                good_photos.append(image_path)
            else:
                bad_photos.append(image_path)

        if SPLIT and len(good_photos) > SPLIT:
            test_photos = good_photos[SPLIT:]
            good_photos = good_photos[:SPLIT]

        print(f"{current_person:<24}: good - {len(good_photos):>5}; bad - {len(bad_photos):>5}; test - {len(test_photos):>5}")

        total_good += good_photos
        total_bad += bad_photos

        for good_photo in good_photos:
            shutil.copy2(good_photo, os.path.join(TRAIN, f"{LABELS[current_person]}.{current_person}"))

        for test_photo in test_photos:
            shutil.copy2(test_photo, os.path.join(TEST, f"{LABELS[current_person]}.{current_person}"))

    print(f"{'Total':<24}: good - {len(total_good):>5}; bad - {len(total_bad):>5}")
