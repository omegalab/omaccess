# -*- coding: utf-8 -*-


from views import IndexView, VisitView

ROUTES = [
    ("GET", "/api", IndexView, "index"),
    ("POST", "/api/visit/add", VisitView, "visit_add")
]
