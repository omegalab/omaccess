# -*- coding: utf-8 -*-

from collections import OrderedDict

import ujson as json
from aiohttp import web

from logger import logger_factory


class ApiView(web.View):
    logger = logger_factory("view.base")

    def parse_url(self):
        resource_id = self.request.match_info.get('id')
        prop = self.request.match_info.get('prop')

        return resource_id, prop

    def response(self, text=None):
        self.logger.debug("request from %s: %s, response: %s", self.peername, self.request.path_qs, text)
        return web.Response(text=text)

    def json_response(self, data, **kwargs):
        self.logger.debug("request from %s: %s, response: %s", self.peername, self.request.path_qs, data)
        return web.json_response(data, dumps=self.complex_encoder, **kwargs)

    def bad_response(self, data=None, status=400):
        self.logger.warning("Bad request from %s: %s, data: %s", self.peername, self.request.path_qs, data)
        return web.Response(status=status)

    def not_found(self, status=404):
        self.logger.warning("Bad request from %s: %s", self.peername, self.request.path_qs)
        return web.Response(status=status)

    @property
    def peername(self):
        return self.request.transport.get_extra_info('peername')

    @staticmethod
    def complex_encoder(data):
        return json.dumps(data, sort_keys=False)


class IndexView(ApiView):
    logger = logger_factory("view.index")

    async def get(self):
        return await self.get_index(self.request)

    async def get_index(self, request):
        json_dict = OrderedDict([
            ('hello', "world")
        ])

        return self.json_response(json_dict)


class VisitView(ApiView):
    logger = logger_factory("view.visit")

    async def post(self):
        return await self.post_visit(self.request)

    async def post_visit(self, request):
        try:
            data = await request.json()

            self.logger.debug("%s", data)
            return self.response(text="OK")

        except ValueError:
            data = await request.read()
            return self.bad_response(data)
