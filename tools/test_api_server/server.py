# -*- coding: utf-8 -*-

import asyncio

from aiohttp import web

from logger import Logger
from routes import ROUTES


class Api(Logger):
    def __init__(self):
        super().__init__()
        self.app = None
        self.handler = None
        self.server = None

    async def start(self, loop):
        self.app = web.Application()

        for route in ROUTES:
            route = self.app.router.add_route(route[0], route[1], route[2], name=route[3])

        self.handler = self.app.make_handler(logger=self._logger)
        self.server = await loop.create_server(self.handler, "0.0.0.0", "8085")
        self._logger.info("API server is listening %s port", self.server.sockets[0].getsockname())

    async def stop(self):
        try:
            self.server.close()
            await self.server.wait_closed()
            await self.handler.shutdown(5)
            await self.app.shutdown()
            await self.app.cleanup()
        except AttributeError:
            pass
