# -*- coding: utf-8 -*-
# pylint: enable=W0611
# pylint: disable=R0903

import logging
import os

LOG_LEVEL = "debug"

log_levels = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warn': logging.WARNING,
    'error': logging.ERROR
}

logging.addLevelName(logging.DEBUG, 'debug')
logging.addLevelName(logging.INFO, 'info ')
logging.addLevelName(logging.WARNING, 'warn ')
logging.addLevelName(logging.ERROR, 'error')

log_level = log_levels.get(LOG_LEVEL, logging.DEBUG)

handler = logging.StreamHandler()

if log_level == logging.DEBUG:
    if os.getppid() == 1:
        formatter = logging.Formatter('[%(levelname)s] [%(thread)d] [%(name)s.%(funcName)s] %(message)s')
    else:
        formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] [%(thread)d] [%(name)s.%(funcName)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
else:
    if os.getppid() == 1:
        formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s')
    else:
        formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

handler.setFormatter(formatter)


for logger_name in ['asyncio']:
    _ = logging.getLogger(logger_name)
    _.setLevel(log_level)
    _.addHandler(handler)


def logger_factory(name):
    name_logger = logging.getLogger(name)

    if not name_logger.hasHandlers():
        name_handler = logging.StreamHandler()
        name_handler.setFormatter(formatter)

        name_logger.setLevel(log_level)
        name_logger.addHandler(name_handler)

    return name_logger


class Logger:
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)

        if not self._logger.hasHandlers():
            self._handler = logging.StreamHandler()
            self._handler.setFormatter(formatter)
            self._logger.setLevel(log_level)
            self._logger.addHandler(self._handler)
