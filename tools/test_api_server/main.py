# -*- coding: utf-8 -*-

import asyncio

from logger import logger_factory
from server import Api

logger = logger_factory("main")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    api = Api()
    logger.info("staring api server")

    loop.run_until_complete(api.start(loop))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(api.stop())
    loop.close()
