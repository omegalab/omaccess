# OmAccess Camera

- [Опции cборки](#Опции%20cборки)
- [Опции камеры](#Опции%20камеры)
- [Опции тренера](#Опции%20тренера)

## Опции cборки

- -D SMART_CAMERA_USE_MYRIAD - "Build for OpenVINO Myriad target" (OFF)
- -D SMART_CAMERA_USE_FPGA - "Build for OpenVINO FPGA target" (OFF)
- -D SMART_CAMERA_USE_RASPICAM - "Build for RaspiCam" (OFF)
- -D SMART_CAMERA_BUILD_APP - "Build Smart Camera main application" (ON)
- -D SMART_CAMERA_BUILD_TRAINER - "Build Smart Camera SVM-classifier trainer" (ON)

## Опции камеры

```text
Usage: smart_camera [params]

 -?, --help, --usage (value:true)  print this message
 -a, --alignment (value:landmarks-regression-retail-0009)  face alignment model name
 --age-gender (value:age-gender-recognition-retail-0013)  age and gender detector model
 --align-margin (value:0)  use face margin when align
 --api-host (value:127.0.0.1)  API host
 --api-port (value:8085)  API port
 --api-root (value:api)  API endpoint
 --api-scheme (value:http)  API scheme (http/https)
 --camera-device, -i (value:0)  video camera device index
 --capture-height, -h (value:480)  camera capture height (multiple of 240)
 --capture-width, -w (value:640)  camera captures width (multiple of 320)
 -d, --detection (value:face-detection-adas-0001)  face detection model name
 --emotions (value:emotions-recognition-retail-0003)  emotions detector model
 --face-margin, -m (value:1.1)  margin to add to face rect
 --models-path, -p (value:/usr/local/omaccess/models)  absolute path to models dir
 -o, --video-out (value:0)  video output (window-0/http-1/stdout-2)
 -r, --recognition (value:face-reidentification-retail-0095)  face recognition model name
 --reset-timer (value:60)  video camera device index
 --svm-cost (value:100)  SVM cost value
 --svm-data  SVM models path
 --svm-degree (value:10)  SVM POLY kernel degree
 --svm-gamma (value:0.001)  SVM RBF kernel gamma
 --svm-kernel (value:rbf)  SVM kernel type
 -v, --verbose (value:3)  logging verbose level (0..3)
```

## Опции тренера

```text
Usage: smart_camera_trainer [params]

 -?, --help, --usage (value:true)  print this message
 -a, --alignment (value:landmarks-regression-retail-0009)  face alignment model name
 --age-gender (value:age-gender-recognition-retail-0013)  age and gender detector model
 --align-margin (value:0)  use face margin when align
 -d, --detection (value:face-detection-adas-0001)  face detection model name
 --emotions (value:emotions-recognition-retail-0003)  emotions detector model
 --face-margin, -m (value:1.1)  margin to add to face rect
 --models-path, -p (value:/usr/local/omaccess/models)  absolute path to models dir
 -o, --output-path  output directory path to store svm-model
 -r, --recognition (value:face-reidentification-retail-0095)  face recognition model name
 --svm-cost (value:100)  SVM cost value
 --svm-data  SVM models path
 --svm-degree (value:10)  SVM POLY kernel degree
 --svm-gamma (value:0.001)  SVM RBF kernel gamma
 --svm-kernel (value:rbf)  SVM kernel type
 --test-data (value:0) split train dataset into train and test ones
 --test-ratio (value:0.90)  input directory path with train images
 --train-path  input directory path with train images
 -v, --verbose (value:3)  logging verbose level (0..3)
```
