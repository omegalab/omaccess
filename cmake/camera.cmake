set(SMART_CAMERA_SRC
  ${SMART_CAMERA_SOURCE_DIR}/camera/main.cc
  ${SMART_CAMERA_SOURCE_DIR}/camera/frame.cc
  ${SMART_CAMERA_SOURCE_DIR}/camera/camera_wrapper.cc
  ${SMART_CAMERA_SOURCE_DIR}/camera/smart_camera.cc
  ${SMART_CAMERA_SOURCE_DIR}/camera/streamer.cc
  ${SMART_CAMERA_SOURCE_DIR}/camera/video_out.cc
  ${SMART_CAMERA_SOURCE_DIR}/aio/io.cc
  ${SMART_CAMERA_SOURCE_DIR}/aio/queue.cc
  ${SMART_CAMERA_SOURCE_DIR}/aio/signal_set.cc
  ${SMART_CAMERA_SOURCE_DIR}/aio/task.cc
  ${SMART_CAMERA_SOURCE_DIR}/aio/timer.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/base_cnn.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_detector.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_aligner.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_recognizer.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_registry.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_svm.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/age_gender_detector.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/emotions_detector.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/helper.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/json.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/logger.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/options.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/rest_client.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/base64.cpp
)


add_executable (${SMART_CAMERA_APP} ${SMART_CAMERA_SRC} ${OMPROTO_SRC})

if (SMART_CAMERA_USE_MYRIAD)
  target_compile_definitions(${SMART_CAMERA_APP} PRIVATE "SMART_CAMERA_USE_MYRIAD")
endif()

if (SMART_CAMERA_USE_FPGA)
  target_compile_definitions(${SMART_CAMERA_APP} PRIVATE "SMART_CAMERA_USE_FPGA")
endif()

if (SMART_CAMERA_USE_RASPICAM)
  target_compile_definitions(${SMART_CAMERA_APP} PRIVATE "SMART_CAMERA_USE_RASPICAM")
endif()

target_include_directories(${SMART_CAMERA_APP} PRIVATE
  ${SMART_CAMERA_SOURCE_DIR}
  ${OpenCV_INCLUDE_DIRS}
  ${CURL_INCLUDE_DIRS}
  ${RapidJSON_INCLUDE_DIRS}
)

target_link_libraries (${SMART_CAMERA_APP} PRIVATE
  fmt::fmt-header-only
  IE::inference_engine
  protobuf::libprotobuf
  Threads::Threads
  ${STD_LIB_WITH_FS}
  ${OpenCV_LIBS}
  ${raspicam_LIBS}
  ${CURL_LIBRARIES}
  ${LINK_CPU_EXTENSIONS})
