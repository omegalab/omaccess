set(SMART_CAMERA_TRAINER_SRC
  ${SMART_CAMERA_SOURCE_DIR}/trainer/main.cc
  ${SMART_CAMERA_SOURCE_DIR}/trainer/smart_camera_trainer.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/base_cnn.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_detector.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_aligner.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_recognizer.cc
  ${SMART_CAMERA_SOURCE_DIR}/dnn/face_svm.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/helper.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/logger.cc
  ${SMART_CAMERA_SOURCE_DIR}/utils/options.cc
)

add_executable (${SMART_CAMERA_TRAINER} ${SMART_CAMERA_TRAINER_SRC} ${OMPROTO_SRC})

if (SMART_CAMERA_USE_MYRIAD)
  target_compile_definitions(${SMART_CAMERA_TRAINER} PRIVATE "SMART_CAMERA_USE_MYRIAD")
endif()

if (SMART_CAMERA_USE_FPGA)
  target_compile_definitions(${SMART_CAMERA_TRAINER} PRIVATE "SMART_CAMERA_USE_FPGA")
endif()

if (SMART_CAMERA_USE_RASPICAM)
  target_compile_definitions(${SMART_CAMERA_TRAINER} PRIVATE "SMART_CAMERA_USE_RASPICAM")
endif()

target_include_directories(${SMART_CAMERA_TRAINER} PRIVATE
  ${SMART_CAMERA_SOURCE_DIR}
  ${OpenCV_INCLUDE_DIRS}
  ${CURL_INCLUDE_DIRS}
  ${RapidJSON_INCLUDE_DIRS}
)

target_link_libraries (${SMART_CAMERA_TRAINER} PRIVATE
  fmt::fmt-header-only
  IE::inference_engine
  protobuf::libprotobuf
  Threads::Threads
  ${STD_LIB_WITH_FS}
  ${CURL_LIBRARIES}
  ${OpenCV_LIBS}
  ${LINK_CPU_EXTENSIONS})

