FROM ubuntu:bionic AS build

ARG OPENVINO_LINK=http://registrationcenter-download.intel.com/akdlm/irc_nas/15944/l_openvino_toolkit_p_2019.3.334.tgz
ARG OPENVINO_COMPONENTS=intel-openvino-ie-rt-cpu-ubuntu-bionic__x86_64;intel-openvino-opencv-lib-ubuntu-bionic__x86_64;intel-openvino-ie-sdk-ubuntu-bionic__x86_64
ARG PROTOBUF_LINK=https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protobuf-cpp-3.6.1.tar.gz
ARG SMART_CAMERA_CMAKE_CMD='cmake -D SMART_CAMERA_BUILD_APP=NO -D SMART_CAMERA_BUILD_TRAINER=ON -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_C_COMPILER=gcc-8 -D CMAKE_CXX_COMPILER=g++-8 ..'

WORKDIR /tmp/omaccess

ADD cmake ./
ADD io ./
ADD ml ./
ADD models ./
ADD protocol ./
ADD smart_camera ./
ADD smart_camera_trainer ./
ADD third_party ./
ADD utils ./
ADD CMakeLists.txt ./

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y wget cpio gcc-8 g++-8 cmake make git autoconf automake libtool unzip libcurl4-openssl-dev python3-pip && \
    wget -cq $OPENVINO_LINK && \
    tar xf l_openvino_toolkit*.tgz && cd l_openvino_toolkit* && \
    sed -i "s/decline/accept/g" silent.cfg && \
    sed -i "s/DEFAULTS/${OPENVINO_COMPONENTS}/g" silent.cfg &&  \
    ./install.sh -s silent.cfg && /opt/intel/openvino/install_dependencies/install_openvino_dependencies.sh && cd .. && \
    wget -cq $PROTOBUF_LINK && \
    tar xf protobuf-cpp-3.6.1.tar.gz && cd protobuf-3.6.1 && \
    ./configure -prefix=/usr/local/protobuf && make -j$(nproc) && make install && cd .. && \
    cp /usr/local/protobuf/bin/protoc /usr/bin/ && \
    cp -vr /usr/local/protobuf/include/google /usr/include/ && \
    cp -vr /usr/local/protobuf/lib/* /usr/lib/ && ldconfig -v && \
    mkdir om_build && cd om_build && \
    /bin/bash -c "source /opt/intel/openvino/bin/setupvars.sh && $SMART_CAMERA_CMAKE_CMD && make -j $(nproc)" && \
    strip -s smart_camera_trainer && cd ../models && pip3 install -r requirements.txt && python3 download_models.py

FROM ubuntu:bionic AS install

ARG OPENVINO_COMPONENTS=intel-openvino-ie-rt-cpu-ubuntu-bionic__x86_64;intel-openvino-opencv-lib-ubuntu-bionic__x86_64

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y cpio  && \
    wget -cq $OPENVINO_LINK && \
    tar xf l_openvino_toolkit*.tgz && cd l_openvino_toolkit* && \
    sed -i "s/decline/accept/g" silent.cfg && \
    sed -i "s/DEFAULTS/$OPENVINO_COMPONENTS/g" silent.cfg &&  \
    ./install.sh -s silent.cfg

FROM ubuntu:bionic

COPY --from=build /usr/local/protobuf/ /usr/
COPY --from=build /tmp/omaccess/models /usr/local/omaccess/
COPY --from=build /tmp/omaccess/om_build/smart_camera_trainer /usr/bin/
COPY --from=build /tmp/omaccess/om_build/ie_cpu_extension/libcpu_extension.so /usr/lib/
COPY --from=install /opt/intel /opt/intel

RUN /opt/intel/openvino/install_dependencies/install_openvino_dependencies.sh && ldconfig -v && \
    /bin/bash -c "source /opt/intel/openvino/bin/setupvars.sh"
